import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map'
import {AppComponent} from "../app.component";

@Injectable()
export class AuthenticationService {

    constructor(private http: HttpClient) { }
    private host : string = "http://vasityprojects.com/schoolx_api/api/";
    // private host : string = "http://localhost:8000/api/";
    // private host : string = "http://sxapi.adrotec.com/api/";
    login(email: string, password: string) {
        return this.http.post<any>(this.host+'login', { email: email, password: password })
            .map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.token) {
                   // store user details and jwt token in local storage to keep user logged in between page refreshes
                   sessionStorage.setItem('token',JSON.stringify(user.token));
                    sessionStorage.setItem('currentUserName',JSON.stringify(user.user["name"]));
                    sessionStorage.setItem('currentUserId',JSON.stringify(user.user["id"]));
                    sessionStorage.setItem('expire_on',JSON.stringify(user.user["expire_on"]));
                    let today = new Date();
                    // let user_expire_date = new Date();
                    // if(JSON.stringify(user.user["expire_on"]) != ''){

                    // }

                    // let formatted_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                    if(JSON.stringify(user.user["expire_on"]) === "null"){
                        sessionStorage.setItem('isExpire', 'false');
                    }else{
                        let user_expire_date = new Date(JSON.stringify(user.user["expire_on"]));
                        if(new Date() > user_expire_date){
                            sessionStorage.setItem('isExpire','true');
                        }else{
                            sessionStorage.setItem('isExpire', 'false');
                        }
                    }


                    console.log(sessionStorage)
                }

                return user;
            });
    }

    public getCurrentUserId(){
        if (sessionStorage.getItem('currentUserId')){
            return JSON.parse(localStorage.getItem('currentUserId'));
        } else return null;
    }
    public getCurrentUserName(){
        if (sessionStorage.getItem('currentUserName')){
            return sessionStorage.getItem('currentUserName');
        } else return "";
    }

    logout() {
        // remove user from local storage to log user out
        sessionStorage.removeItem('token');
       sessionStorage.removeItem('currentUserName');
        sessionStorage.removeItem('currentUserId');

    }
}
