import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DomSanitizer} from '@angular/platform-browser';
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class DataserviceService {
    private userId: number = 0
    //private host : string = "http://api.schoolxlk.tk/api/";
    private env = environment
    private host: string = this.env.apiUrl;
    json: JSON;

    constructor(private http: HttpClient, private sanitizer: DomSanitizer) {
    }

    getSafeUrl(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url)
    }

    getBreadCrumbs() {
        return breadcrumb;
    }

    setGrade(id, name) {
        breadcrumb.id_g = id;
        breadcrumb.name_g = name;
        this.setSubject(0, "-");
        this.setChapter(0, "-");
        this.setModule(0, "-");
    }

    setSubject(id, name) {
        breadcrumb.id_s = id;
        breadcrumb.name_s = name;
        this.setChapter(0, "-");
        this.setModule(0, "-");
    }

    setChapter(id, name) {
        breadcrumb.id_c = id;
        breadcrumb.name_c = name;
        this.setModule(0, "-");
    }

    setModule(id, name) {
        breadcrumb.id_m = id;
        breadcrumb.name_m = name;
    }

    setModuleRating(id, rating) {
        return this.http.get<dataOut>(this.host + "modulerate/" + id + "/" + sessionStorage.getItem('currentUserId') + "/" + rating);
    }

    setVideosRating(id, rating): Observable<any> {
        return this.http.get<dataOut>(this.host + "videorate/" + id + "/" + sessionStorage.getItem('currentUserId') + "/" + rating);
    }

    getGrades(): Observable<any> {
        return this.http.get<any>(this.host + "grades/all");
    }

    getSubjects(id, userId): Observable<any> {
        return this.http.get<any>(this.host + "subjects/get/grade/" + id + "/" + userId);
    }

    getChapters(id, userId): Observable<any> {
        return this.http.get<any>(this.host + "chapters/get/subject/" + id + "/" + userId);
    }

    getModules(id, userId): Observable<any> {
        //json:JSON=JSON.parse([{id:7,video:"https://www.youtube.com/watch?v=FPhetlu3f2g",name:"Another Video",cun:31,und:30,con:21,dun:19}])
        //return this.json;
        return this.http.get<any>(this.host + "modules/get/chapter/" + id + "/" + userId);
    }

    getModuleVideos(id, userId): Observable<any> {
        return this.http.get<any>(this.host + "materials/get/module/" + id + "/" + userId);
    }

    getPoints(userId): Observable<any> {
        return this.http.get<any>(this.host + "points/get/" + userId);
    }

    getPackages(): Observable<any> {
        return this.http.get<any>(this.host + "packages/get/all");
    }

    selectPckageSave(chapter_id:number, package_id: number,userId) {
        // console.log(chapter_id);
        return this.http.get<any>(this.host + "package/select/"+chapter_id+"/"+package_id+"/"+userId);
    }
}

const breadcrumb = {
    "id_g": 0,
    "id_s": 0,
    "id_c": 0,
    "id_m": 0,

    "name_g": "-",
    "name_s": "-",
    "name_c": "-",
    "name_m": "-"
};


export interface BreadCrumb {
    label: string;
    url: string;
}
;

export interface dataOut {

}

export interface IBreadcrumb {
    text: string,  // The text to display
    path: string   // The associated path
}