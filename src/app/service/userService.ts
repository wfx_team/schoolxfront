import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from  'src/app/model/user';
import {environment} from "../../environments/environment";


@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }
    private env=environment
    private host : string = this.env.apiUrl;
    getAll() {
        return this.http.get<User[]>('/api/users');
    }

    getById(id: number) {
        return this.http.get('/api/users/' + id);
    }

    create(user: User) {
        return this.http.post(this.host+'register1', user);
    }

    update(user: User) {
        return this.http.put('/api/users/' + user.id, user);
    }

    delete(id: number) {
        return this.http.delete('/api/users/' + id);
    }
}