import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataserviceService } from 'src/app/service/dataservice.service';
import {AppComponent} from "../../app.component";

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styles: []
})
export class SubjectComponent implements OnInit {
  public subjects = [ ];
  userId:number=0;
  constructor(private route:ActivatedRoute, 
    private router:Router,
    private _dataservice: DataserviceService,private app:AppComponent) {
    this.app.userName=sessionStorage.getItem('currentUserName');
    this.app.session=sessionStorage.getItem('token');
     app.breadcrumb.id_s=0
    app.breadcrumb.id_c=0
    app.breadcrumb.id_m=0
    this._dataservice.getBreadCrumbs();
    if(sessionStorage.getItem('currentUserId')){
      this.userId=parseInt(sessionStorage.getItem('currentUserId'));
    }
  }

  ngOnInit() {

    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this._dataservice.getSubjects(id,this.userId).subscribe(data=>{
        this.subjects = data;
    });

    if(this._dataservice.getBreadCrumbs().id_g!==id){

      this.router.navigate(['/home']);
    }
  }

  onSelect(sel){
    this.router.navigate(['/chap',sel.is]);
    this._dataservice.setSubject(sel.is, sel.name);
  }


  safeUrl(url){
    return this._dataservice.getSafeUrl(url);
  }
}
