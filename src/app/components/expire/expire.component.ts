import { Component, OnInit } from '@angular/core';
import {  ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';

@Component({
  // selector: 'expire',
  templateUrl: './expire.component.html',
  // styleUrls: ['./expire.component.scss']
})
export class ExpireComponent implements OnInit {
  expire ='false';
  public expire_on = '';
  constructor(private router:Router,
              private route: ActivatedRoute,
              public app : AppComponent)
  {
    this.app.session=sessionStorage.getItem('token');
    if(sessionStorage.getItem('isExpire')){
      this.expire=sessionStorage.getItem('isExpire');
    }
    if(sessionStorage.getItem('expire_on')){
      this.expire_on=sessionStorage.getItem('expire_on');
    }
  }

  ngOnInit() {

    if(!this.expire === true){
      this.router.navigate(['/login']);
      location.reload();
    }
  }

}
