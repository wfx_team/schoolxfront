import {Component, OnInit, Inject, Input, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {DataserviceService} from 'src/app/service/dataservice.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponent} from "../../app.component";

export interface DialogData {

}

@Component({
    selector: 'app-chapter',
    templateUrl: './chapter.component.html',
})
export class ChapterComponent implements OnInit {
    public chapter = [];
    userId: number = 0;
    // public points = 0;


    constructor(private route: ActivatedRoute,
                public dialog: MatDialog,
                private router: Router,
                private _dataservice: DataserviceService, private app: AppComponent) {
        this.app.userName = sessionStorage.getItem('currentUserName');
        this.app.session = sessionStorage.getItem('token');
        // app.breadcrumb.id_c = 0
        // app.breadcrumb.id_m = 0;
        this._dataservice.getBreadCrumbs();
        if (sessionStorage.getItem('currentUserId')) {
            this.userId = parseInt(sessionStorage.getItem('currentUserId'));
            // this._dataservice.getPoints(sessionStorage.getItem('currentUserId')).subscribe(data => {
            //     this.points = data.point_balance;
            //     console.log(data.point_balance);
            // });
        }

    }

    ngOnInit() {
        console.log(sessionStorage);
        let id = parseInt(this.route.snapshot.paramMap.get('id'));
        this._dataservice.getChapters(id, this.userId).subscribe(data => {
            this.chapter = data;
        });
        console.log(id);
        console.log(this._dataservice.getBreadCrumbs());
        console.log(this._dataservice.getBreadCrumbs().id_s);
        if (this._dataservice.getBreadCrumbs().id_s != id) {
            this.router.navigate(['/home']);
        }
    }

    onSelect(sel) {
        console.log(sel);
        this._dataservice.setChapter(sel.id, sel.name);
        if (sessionStorage.getItem('currentUserId')) {
            // if(sessionStorage.getItem('packageName') == null || sessionStorage.getItem('isPackageExpired')=="true"){
            if(sel.is_unlock <= 0){
                this.openDialog(sel);
            }else{
                this._dataservice.setChapter(sel.id, sel.name);
                this.router.navigate(['/mod', sel.id]);
            }

        } else {
            this.router.navigate(['/mod', sel.id]);
        }

        // this.router.navigate(['/mod', sel.id]);
    }

    setSubject() {

    }

    safeUrl(url) {
        return this._dataservice.getSafeUrl(url);
    }

// =====================

    openDialog(sel): void {

        const dialogRef = this.dialog.open(PopUp, {
            disableClose: true,
            minWidth: '60%',
            minHeight: '40%',
            maxWidth: '90%',
            maxHeight: '90%',
            data: sel
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');

        });
    }
}

@Component({
    selector: 'pop-up',
    templateUrl: 'pop-dialog.html',
})
export class PopUp implements OnInit {
    isRated: boolean;
    rating: number;
    public noty: string = '';
    // public points = 0;
    public packages;
    public chapterData;
    constructor(public dialogRef: MatDialogRef<PopUp>,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private _dataservice: DataserviceService, private router: Router) {
        this.chapterData =data;


    }

    ngOnInit() {
        this._dataservice.getPackages().subscribe(result => {
            this.packages = result;
            console.log(result);
        });

        if (sessionStorage.getItem('currentUserId')) {
            this._dataservice.getPoints(sessionStorage.getItem('currentUserId')).subscribe(data => {
                // this.points = data.point_balance;
                console.log(data.point_balance);
            });
        }
    }

    selectPackage(x) {
        this._dataservice.selectPckageSave(this.chapterData.id, x, sessionStorage.getItem('currentUserId')).subscribe(data => {
            console.log(data.userDetails.point_balance);
            // this.app.points =data.userDetails.point_balance;
            if (data.status == true) {
                this.noty = '<div class="alert alert-success alert-dismissible">' +
                    data.message +
                    '</div>';
                setTimeout(function () {
                    // this.router.navigate(['/home']);
                    location.reload(true);
                },3000);
            }else{
                this.noty = '<div class="alert alert-danger alert-dismissible">' +
                    data.message +
                    '</div>';
            }
        });
        console.log(x);
    }

}
