import { Component, OnInit } from '@angular/core';
import { DataserviceService } from 'src/app/service/dataservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import {AppComponent} from "../../app.component";

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styles: []
})
export class ModuleComponent implements OnInit {

  public module = [];
  userId:number=0;
  constructor( private route:ActivatedRoute, 
    private router:Router,
    private _dataservice: DataserviceService,private app:AppComponent ) {
    app.breadcrumb.id_m=0
    this.app.userName=sessionStorage.getItem('currentUserName');
    this.app.session=sessionStorage.getItem('token');
    if(sessionStorage.getItem('currentUserId')){
      this.userId=parseInt(sessionStorage.getItem('currentUserId'));
    }
  }

  ngOnInit() {

    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this._dataservice.getModules(id,this.userId).subscribe(data=>{
      console.log(data)
      this.module = data;
    });
console.log(this._dataservice.getBreadCrumbs());
    if(this._dataservice.getBreadCrumbs().id_c!==id){
      // this.router.navigate(['/home']);
    }
  }

  onSelect(sel){
    this.router.navigate(['/modv',sel.id]);
    this._dataservice.setModule(sel.id, sel.name);
  }

  safeUrl(url){
    return this._dataservice.getSafeUrl(url);
  }
}
