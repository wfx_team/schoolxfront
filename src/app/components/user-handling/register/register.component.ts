import { Component } from '@angular/core';
import { Router } from '@angular/router';


import { AlertService} from 'src/app/service/alert.service';
import {UserService} from "../../../service/userService";

@Component({
 // moduleId: module.id,
  templateUrl: 'register.component.html'
})

export class RegisterComponent {
    error_msg = '';
    success_msg = '';
  model: any = {};
  loading = false;
    errorSet:any={errors:{}};
    emailError:string="";
  constructor(
      private router: Router,
      private userService: UserService,
      private alertService: AlertService) { }

  register() {
    this.loading = true;
    this.userService.create(this.model)
        .subscribe(
            data => {
                console.log(data);
               this.errorSet=data;
                    ///this.emailError =this.errorSet.errors.email[0];

                if(this.errorSet == 'true'){
                    this.success_msg = "Registration successful. Please Login to system.";
                    this.error_msg ="";
                        setTimeout (() => {
                        this.router.navigate(['/login']);
                    }, 4000);

                }else{
                    this.success_msg ="";
                    // console.log("lllll");
                    // this.alertService.error('Email already exist...', true);
                    this.error_msg = 'Email already exist...';
                }
                this.loading = false;
               // sessionStorage.setItem('token',data.token)
              // set success message and pass true paramater to persist the message after redirecting to the login page
             // this.alertService.success('Registration successful', true);
             // this.router.navigate(['/login']);
            },
            error => {
              this.alertService.error(error);
              this.loading = false;
            });
  }
}

