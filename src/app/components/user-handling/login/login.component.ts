import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {AuthenticationService} from 'src/app/service/authentication.service';
import {AlertService} from 'src/app/service/alert.service';
import {AppComponent} from "../../../app.component";


@Component({
    //moduleId: module.id,
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    error_msg = '';

    constructor(private route: ActivatedRoute,
                private router: Router,
                private app: AppComponent,
                private authenticationService: AuthenticationService,
                private alertService: AlertService) {

    }

    ngOnInit() {
        // reset login status
        // this.authenticationService.logout();
        this.returnUrl = '/home';
        if (sessionStorage.getItem('currentUserId')) {
            // get return url from route parameters or default to '/'
            this.router.navigate([this.returnUrl]);
        }
        // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
    }

    login() {
        this.loading = true;
        this.authenticationService.login(this.model.email, this.model.password)
            .subscribe(
                data => {
                    location.reload(true);
                    // window.location='http://schoolx.vasityprojects.com/home';
                    // this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                    this.error_msg = 'Invalid credentials. Please try again.';
                });
    }

}