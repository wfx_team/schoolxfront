import {Component, OnInit, Inject, Input, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {DataserviceService} from 'src/app/service/dataservice.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponent} from 'src/app/app.component';
import {split} from "ts-node/dist";

export interface DialogData {
    id: number;
    name: string;
    video: string;
    tutor: string;
    cun: number;
    und: number;
    con: number;
    dun: number;
}

@Component({
    selector: 'app-module-videos',
    templateUrl: './module-videos.component.html',
    styles: []
})
export class ModuleVideosComponent implements OnInit {
    public modulevideo = [];
    public module = 1;
    isRated: boolean;
    rating: number;
    userId: number = 0;
    expire = 'false';

    constructor(public dialog: MatDialog,
                private router: Router,
                private route: ActivatedRoute,
                private _dataservice: DataserviceService,
                public app: AppComponent) {
        this.app.userName = sessionStorage.getItem('currentUserName').replace(/['"]+/g, '');
        this.app.session = sessionStorage.getItem('token');
        if (sessionStorage.getItem('currentUserId')) {
            this.userId = parseInt(sessionStorage.getItem('currentUserId'));
        }
        // if (sessionStorage.getItem('isExpire')) {
        //     this.expire = sessionStorage.getItem('isExpire');
        // }
    }

    ngOnInit() {
        this.isRated = false;
        console.log(sessionStorage);

        // if (this.expire === 'true') {
        //     this.router.navigate(['/expire']);
        //     // location.reload();
        // }
        let id = parseInt(this.route.snapshot.paramMap.get('id'));
        this._dataservice.getModuleVideos(id, this.userId).subscribe(data => {
            this.modulevideo = data;
            console.log(data);

        });
        this.module = id
        if (this._dataservice.getBreadCrumbs().id_m !== id) {
            // this.router.navigate(['/home']);

        }

    }

    openDialog(sel): void {

        const dialogRef = this.dialog.open(VideoPlayback, {
            disableClose: true,
            minWidth: '60%',
            minHeight: '40%',
            maxWidth: '90%',
            maxHeight: '90%',
            data: sel
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            let id = parseInt(this.route.snapshot.paramMap.get('id'));
            this._dataservice.getModuleVideos(id, this.userId).subscribe(data => {
                this.modulevideo = data;
            });
        });
    }

    onRatingModuleChanged(rating) {

        this.isRated = true;
        this._dataservice.setModuleRating(this.module, rating).subscribe(result => {
            console.log(result);
        });
    }
}


@Component({
    selector: 'video-playback',
    templateUrl: 'video-dialog.html',
})
export class VideoPlayback implements OnInit {

    isRated: boolean;
    rating: number;

    public videoSrc;
    modulevideo: DialogData;

    constructor(public dialogRef: MatDialogRef<VideoPlayback>,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private _dataservice: DataserviceService, private router: Router) {
    }

    ngOnInit() {
        this.isRated = false;
        this.modulevideo = this.data;


        this.videoSrc = this._dataservice.getSafeUrl('https://www.youtube.com/embed/' + this.modulevideo.video.split("https://www.youtube.com/watch?v=").pop());
    }

    onRatingVideoChanged(rating) {
        this.isRated = true;
        this._dataservice.setVideosRating(this.modulevideo.id, rating).subscribe(result => {
            console.log(result);
        });
        //  this.router.navigate([['/mod',this.modulevideo.id]]);
    }


}
