import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataserviceService } from 'src/app/service/dataservice.service';
import {AppComponent} from "../../app.component";

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {
  breadcrumb = {};
  grade:string =""
  constructor(private route:ActivatedRoute,
    private router:Router,
    private _dataservice: DataserviceService,private app:AppComponent) {
    this.app.userName=sessionStorage.getItem('currentUserName');
    this.app.session=sessionStorage.getItem('token');
    app.breadcrumb.id_g=0
    app.breadcrumb.id_s=0
    app.breadcrumb.id_c=0
    app.breadcrumb.id_m=0
  }

  ngOnInit() {
    this._dataservice.getBreadCrumbs();
  }

  onSelect(id){

    this.router.navigate(['/sub',id]);
    if(id==1){
      this.grade="8";
    }else if(id==2){
      this.grade="9";
    }
    else if(id==3){
      this.grade="10";
    }
    else if(id==4){
      this.grade="11";
    }
    this._dataservice.setGrade(id, "Grade "+this.grade);

  }

}
