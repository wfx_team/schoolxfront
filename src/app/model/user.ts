export class User {
    id: number;
    name: string;
    password: string;
    email: string;
    mobile: string;
    price:number;
}
