import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

//Angular Material Components
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCheckboxModule} from '@angular/material';
import {MatButtonModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatMenuModule} from '@angular/material/menu';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import {MatTabsModule} from '@angular/material/tabs';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';

import {AppRoutingModule, routingComponents} from './app-routing.module';
import {AppComponent} from './app.component';
import {DataserviceService} from './service/dataservice.service';
import {VideoPlayback} from './components/module-videos/module-videos.component';
import {HttpClientModule} from '@angular/common/http';
import {AngularFontAwesomeModule} from "angular-font-awesome";
import {AuthGuard} from "./guards/auth.guard";
import {AuthenticationService} from "./service/authentication.service";
import {UserService} from "./service/userService";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor} from "./helpers/jwt.interceptor";
import {fakeBackendProvider} from "./helpers/fake-backend";
import {LoginComponent} from "./components/user-handling/login/login.component";
import {RegisterComponent} from "./components/user-handling/register/register.component";
import {FormsModule} from "@angular/forms";
import {AlertService} from "./service/alert.service";
import {AlertComponent} from "./components/alert/alert.component";
import {McBreadcrumbsModule} from "ngx-breadcrumbs";
import {ReplaceLineBreaks} from "./helpers/textpipe";
import {ValidationMsgPipe} from "./helpers/validationpipe";
import {ExpireComponent} from "./components/expire/expire.component";
import {PopUp} from "./components/chapter/chapter.component";
// import {StarRatingComponent} from "./extraComponents/star-rating/star-rating.component";
// import {StarRatingDisplayComponent} from "./extraComponents/star-rating/StarRatingDisplayComponent";
// import { DeviceDetectorModule } from 'ngx-device-detector';
@NgModule({
    declarations: [
        AppComponent,
        routingComponents,
        //fakeBackendProvider,
        VideoPlayback,
        AlertComponent,
        LoginComponent,
        RegisterComponent,
        ReplaceLineBreaks,
        ValidationMsgPipe,
        ExpireComponent,
        PopUp
        // StarRatingComponent,
        // StarRatingDisplayComponent
        //ScrollDispatchModule,
        // McBreadcrumbsModule.forRoot()
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MatCheckboxModule,
        MatCheckboxModule,
        MatButtonModule,
        MatInputModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatRadioModule,
        MatSelectModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatMenuModule,
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        MatGridListModule,
        MatCardModule,
        MatStepperModule,
        MatTabsModule,
        MatExpansionModule,
        MatButtonToggleModule,
        MatChipsModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSnackBarModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        AngularFontAwesomeModule,
        AppRoutingModule,
        HttpClientModule,
        ScrollingModule,
        // DeviceDetectorModule.forRoot(),
        ScrollDispatchModule

    ],
    entryComponents: [
        VideoPlayback,
        PopUp
    ],
    providers: [
        DataserviceService,
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },

        // provider used to create fake backend
        //fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
