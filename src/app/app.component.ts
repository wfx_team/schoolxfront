import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {DataserviceService} from './service/dataservice.service';
import {AuthenticationService} from "./service/authentication.service";
import {Router, NavigationEnd} from "@angular/router";
import {Observable} from "rxjs";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {

    title = 'SCHOOLX';
    public breadcrumb: any;
    mobileQuery: MediaQueryList;
    public session;
    public toolBar = 0;
    public userName;
    public points =0;
    public packagename = 'trail';
    public isPackageExpired:boolean = false;
    name: string;
    menu: Array<any> = [];
    breadcrumbList: Array<any> = [];
    private _mobileQueryListener: () => void;

    constructor(changeDetectorRef: ChangeDetectorRef,
                media: MediaMatcher,
                private _dataservice: DataserviceService,
                private  _auth: AuthenticationService, private router: Router) {
        this.mobileQuery = media.matchMedia('(max-width: 900px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);

        this.breadcrumb = this._dataservice.getBreadCrumbs()
        this.breadcrumb.id_g = 0
        this.breadcrumb.id_s = 0
        this.breadcrumb.id_c = 0
        this.breadcrumb.id_m = 0
        if (sessionStorage != null) {
            this.session = sessionStorage.getItem("token");
            this.userName = _auth.getCurrentUserName();
            this._dataservice.getPoints(sessionStorage.getItem('currentUserId')).subscribe(data => {
                this.points = data.point_balance;
                console.log(data.point_balance);
            });
        } else {
            this.session = null;
            this.userName = '';
        }
    }
    ngOnInit() {
        if (sessionStorage.getItem('currentUserId') != null) {
            // this.points = this._dataservice.getPoints();
            this._dataservice.getPoints(sessionStorage.getItem('currentUserId')).subscribe(data => {
                this.points = data.point_balance;
                console.log(data.point_balance);
                if(new Date() > new Date(data.points_expire_on)){
                    sessionStorage.setItem('isPackageExpired','true');
                    sessionStorage.setItem('packageName','paid');
                }else{
                    sessionStorage.setItem('isPackageExpired', 'false');
                    sessionStorage.setItem('packageName','paid');
                }
            });
        }else{
            sessionStorage.setItem('packageName','trail');
        }
    }
    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    }

    logout() {
        this._auth.logout();

        this.router.navigate(['home']);
        location.reload()
    }

    setSubject(name) {
        // this._dataservice.setSubject(id,name);
        console.log(name)
        console.log(this.breadcrumb)
        //this.breadcrumb = this._dataservice.getBreadCrumbs()
        console.log(this.breadcrumb)
    };
}
