(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule, routingComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routingComponents", function() { return routingComponents; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_subject_subject_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/subject/subject.component */ "./src/app/components/subject/subject.component.ts");
/* harmony import */ var _components_chapter_chapter_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/chapter/chapter.component */ "./src/app/components/chapter/chapter.component.ts");
/* harmony import */ var _components_module_module_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/module/module.component */ "./src/app/components/module/module.component.ts");
/* harmony import */ var _components_module_videos_module_videos_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/module-videos/module-videos.component */ "./src/app/components/module-videos/module-videos.component.ts");
/* harmony import */ var _components_user_handling_login_login_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/user-handling/login/login.component */ "./src/app/components/user-handling/login/login.component.ts");
/* harmony import */ var _components_expire_expire_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/expire/expire.component */ "./src/app/components/expire/expire.component.ts");
/* harmony import */ var _components_user_handling_register_register_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/user-handling/register/register.component */ "./src/app/components/user-handling/register/register.component.ts");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./guards/auth.guard */ "./src/app/guards/auth.guard.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");













var routingCompo = [
    _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"],
    _components_subject_subject_component__WEBPACK_IMPORTED_MODULE_4__["SubjectComponent"],
    _components_chapter_chapter_component__WEBPACK_IMPORTED_MODULE_5__["ChapterComponent"],
    _components_module_module_component__WEBPACK_IMPORTED_MODULE_6__["ModuleComponent"],
    _components_user_handling_login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"],
    _components_user_handling_register_register_component__WEBPACK_IMPORTED_MODULE_10__["RegisterComponent"],
    _components_module_videos_module_videos_component__WEBPACK_IMPORTED_MODULE_7__["ModuleVideosComponent"],
    _components_expire_expire_component__WEBPACK_IMPORTED_MODULE_9__["ExpireComponent"],
];
var routes = [
    { path: '', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    { path: 'login', component: _components_user_handling_login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"] },
    { path: 'register', component: _components_user_handling_register_register_component__WEBPACK_IMPORTED_MODULE_10__["RegisterComponent"] },
    { path: 'home', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    { path: 'sub/:id', component: _components_subject_subject_component__WEBPACK_IMPORTED_MODULE_4__["SubjectComponent"], runGuardsAndResolvers: 'always' },
    { path: 'chap/:id', component: _components_chapter_chapter_component__WEBPACK_IMPORTED_MODULE_5__["ChapterComponent"] },
    { path: 'mod/:id', component: _components_module_module_component__WEBPACK_IMPORTED_MODULE_6__["ModuleComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_11__["AuthGuard"]] },
    { path: 'modv/:id', component: _components_module_videos_module_videos_component__WEBPACK_IMPORTED_MODULE_7__["ModuleVideosComponent"], canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_11__["AuthGuard"]] },
    { path: 'expire', component: _components_expire_expire_component__WEBPACK_IMPORTED_MODULE_9__["ExpireComponent"] },
    { path: '**', redirectTo: 'home' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { onSameUrlNavigation: 'reload' })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            providers: [{ provide: _angular_common__WEBPACK_IMPORTED_MODULE_12__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_12__["HashLocationStrategy"] }],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());

var routingComponents = routingCompo;


/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container\" [class.example-is-mobile]=\"mobileQuery.matches\">\r\n\r\n    <mat-toolbar color=\"primary\" class=\"example-toolbar\" *ngIf=\"mobileQuery.matches\">\r\n            <button mat-icon-button (click)=\"snav.toggle()\"><mat-icon>menu</mat-icon></button>\r\n            <h1 class=\"example-app-name\">SchoolX</h1>\r\n    </mat-toolbar>\r\n\r\n    <mat-toolbar color=\"primary\" class=\"nav-main\" *ngIf=\"!(mobileQuery.matches)\">\r\n        <span>\r\n            <a routerLinkActive=\"active\"\r\n            routerLink=\"/home\" mat-button style=\"font-size:160%\">SchoolX</a>\r\n             <!--(click)=\"hide()\"-->\r\n        </span>\r\n        <span style=\"flex: 1 1 auto;\" >\r\n\r\n            <a mat-button routerLinkActive=\"active\" *ngIf=\"breadcrumb.id_g>0\"\r\n            routerLink=\"/sub/{{breadcrumb.id_g}}\"><mat-icon>arrow_right</mat-icon> {{breadcrumb.name_g}}</a>\r\n\r\n            <a mat-button routerLinkActive=\"active\" *ngIf=\"breadcrumb.id_s>0\"\r\n            routerLink=\"/chap/{{breadcrumb.id_s}}\" ><mat-icon>arrow_right</mat-icon> {{breadcrumb.name_s}}</a>\r\n\r\n            <a mat-button routerLinkActive=\"active\" *ngIf=\"breadcrumb.id_c>0\"\r\n            routerLink=\"/mod/{{breadcrumb.id_c}}\"><mat-icon>arrow_right</mat-icon> {{breadcrumb.name_c}}</a>\r\n\r\n<!--            <a mat-button routerLinkActive=\"active\" *ngIf=\"breadcrumb.id_m>0\"\r\n            routerLink=\"/modv/{{breadcrumb.id_m}}\"><mat-icon>arrow_right</mat-icon> {{breadcrumb.name_m}}</a> -->\r\n\r\n        </span>\r\n        <!--<ng-container *ngIf=\"!router.url.includes('login')|| !router.url.includes('register')\">-->\r\n         <span *ngIf=\"session==null\" >\r\n            <button mat-button [matMenuTriggerFor]=\"userMenu\" ><i class=\"material-icons\"></i>Login</button>\r\n        </span>\r\n            <span *ngIf=\"session!=null\">\r\n            <button mat-button [matMenuTriggerFor]=\"userLogOut\" ><i class=\"material-icons\"></i>{{userName|replaceLineBreaks}}</button>\r\n            <button class=\"point-btn\" ><i class=\"material-icons\"></i>points {{points}}</button>\r\n\r\n        </span>\r\n        <!--</ng-container>-->\r\n\r\n\r\n    </mat-toolbar>\r\n  \r\n    <mat-sidenav-container color=\"primary\" [style.marginTop.px]=\"mobileQuery.matches ? 56 : 0\">\r\n      <mat-sidenav #snav\r\n                   [fixedInViewport]=\"true\" fixedTopGap=\"56\">\r\n          <!--[mode]=\"over\"-->\r\n        <mat-nav-list>\r\n                <a mat-list-item routerLinkActive=\"active\" (click)=\"snav.toggle()\"\r\n                routerLink=\"/home\"><mat-icon>home</mat-icon> Home</a>\r\n\r\n                <a mat-list-item routerLinkActive=\"active\" (click)=\"snav.toggle()\" *ngIf=\"breadcrumb.id_g>0\"\r\n                   routerLink=\"/sub/{{breadcrumb.id_g}}\"><mat-icon>arrow_right</mat-icon>{{breadcrumb.name_g}}</a>\r\n\r\n                <a mat-list-item routerLinkActive=\"active\" (click)=\"snav.toggle()\" *ngIf=\"breadcrumb.id_s>0\"\r\n                routerLink=\"/chap/{{breadcrumb.id_s}}\"><mat-icon>arrow_right</mat-icon> {{breadcrumb.name_s}}</a>\r\n        \r\n                <a mat-list-item routerLinkActive=\"active\" (click)=\"snav.toggle()\" *ngIf=\"breadcrumb.id_c>0\"\r\n                routerLink=\"/mod/{{breadcrumb.id_c}}\"><mat-icon>arrow_right</mat-icon> {{breadcrumb.name_c}}</a>\r\n\r\n<!--                <a mat-list-item routerLinkActive=\"active\" *ngIf=\"breadcrumb.id_m>0\"\r\n                routerLink=\"/modv\"><mat-icon>arrow_right</mat-icon> {{breadcrumb.name_m}}</a> -->\r\n            <span *ngIf=\"session==null\" >\r\n            <button mat-button  [matMenuTriggerFor]=\"userMenu\" ><i class=\"material-icons\"></i>Login</button>\r\n        </span>\r\n            <span *ngIf=\"session!=null\">\r\n            <button mat-button  [matMenuTriggerFor]=\"userLogOut\" ><i class=\"fa fa-user\"></i>{{userName|replaceLineBreaks}}</button>\r\n        </span>\r\n               <!-- <a mat-list-item [matMenuTriggerFor]=\"userMenu\" ><mat-icon>face</mat-icon> User Name</a>-->\r\n\r\n        </mat-nav-list>\r\n      </mat-sidenav>\r\n  \r\n      <mat-sidenav-content color=\"primary\">\r\n          <div *ngIf=\"!mobileQuery.matches\" style=\"margin-top:64px;\"></div>\r\n           <div style=\"height: calc(100% - 0);\">\r\n                <router-outlet></router-outlet>\r\n           </div>\r\n      </mat-sidenav-content>\r\n    </mat-sidenav-container>\r\n</div>\r\n \r\n\r\n<mat-menu #userMenu=\"matMenu\">\r\n    <a [routerLink]=\"['/login']\" mat-menu-item>Login</a>\r\n    <a [routerLink]=\"['/register']\" mat-menu-item>Register</a>\r\n</mat-menu>\r\n<mat-menu #userLogOut=\"matMenu\">\r\n    <a (click)=\"logout()\"  mat-menu-item>Logout</a>\r\n</mat-menu>\r\n\r\n<br>\r\n\r\n<footer>\r\n    <span>\r\n        <a href=\"http://www.schoolx.lk/aboutus/\" style=\"color: green\">About us</a>\r\n    </span>     \r\n    |\r\n    <span>\r\n        <a href=\"http://www.schoolx.lk/tnc/\" style=\"color: green\">T&C</a>\r\n    </span> \r\n    |\r\n    <span>\r\n        <a href=\"http://www.schoolx.lk/contact/\" style=\"color: green\">Contact</a>\r\n    </span>\r\n    |\r\n    <span>\r\n        <a href=\"https://goo.gl/forms/jBvb2yhEF25WDIJn2\" target=\"_blank\" style=\"color: green\">Payments</a>\r\n    </span>\r\n\r\n    <span class=\"pull-right\">\r\n        SchoolX &copy; 2018\r\n    </span> \r\n</footer>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".hide-me {\n  display: none; }\n\nfooter {\n  padding-top: 10px; }\n\n.point-btn {\n  font-size: 13px;\n  padding: 0px 10px;\n  border-radius: 5px;\n  outline: none;\n  box-shadow: 0 0 0;\n  color: #111;\n  background: #2ad322;\n  border: 0px;\n  font-weight: 700; }\n\n/*\r\n.mat-sidenav-fixed {\r\n  background-color: black;\r\n  color: green;\r\n}\r\n.mat-list-item{\r\n  color: green;\r\n}\r\n.active{\r\n  color: white;\r\n}*/\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvRjpcXHdlYiBkZXZlbG9wbWVudFxcd2Z4XFxzY2hvb2x4bGtcXGZyb250L3NyY1xcYXBwXFxhcHAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFhLEVBQ2Q7O0FBQ0Q7RUFDRSxrQkFBaUIsRUFDbEI7O0FBRUQ7RUFDRSxnQkFBZTtFQUNmLGtCQUFpQjtFQUNqQixtQkFBa0I7RUFDbEIsY0FBYTtFQUNiLGtCQUFpQjtFQUNqQixZQUFXO0VBQ1gsb0JBQW1CO0VBQ25CLFlBQVc7RUFDWCxpQkFBZ0IsRUFDakI7O0FBQ0Q7Ozs7Ozs7Ozs7R0FVRyIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oaWRlLW1le1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuZm9vdGVye1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG59XHJcblxyXG4ucG9pbnQtYnRue1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBwYWRkaW5nOiAwcHggMTBweDtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICBib3gtc2hhZG93OiAwIDAgMDtcclxuICBjb2xvcjogIzExMTtcclxuICBiYWNrZ3JvdW5kOiAjMmFkMzIyO1xyXG4gIGJvcmRlcjogMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuLypcclxuLm1hdC1zaWRlbmF2LWZpeGVkIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICBjb2xvcjogZ3JlZW47XHJcbn1cclxuLm1hdC1saXN0LWl0ZW17XHJcbiAgY29sb3I6IGdyZWVuO1xyXG59XHJcbi5hY3RpdmV7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59Ki9cclxuIl19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./service/dataservice.service */ "./src/app/service/dataservice.service.ts");
/* harmony import */ var _service_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./service/authentication.service */ "./src/app/service/authentication.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var AppComponent = /** @class */ (function () {
    function AppComponent(changeDetectorRef, media, _dataservice, _auth, router) {
        var _this = this;
        this._dataservice = _dataservice;
        this._auth = _auth;
        this.router = router;
        this.title = 'SCHOOLX';
        this.toolBar = 0;
        this.points = 0;
        this.packagename = 'trail';
        this.isPackageExpired = false;
        this.menu = [];
        this.breadcrumbList = [];
        this.mobileQuery = media.matchMedia('(max-width: 900px)');
        this._mobileQueryListener = function () { return changeDetectorRef.detectChanges(); };
        this.mobileQuery.addListener(this._mobileQueryListener);
        this.breadcrumb = this._dataservice.getBreadCrumbs();
        this.breadcrumb.id_g = 0;
        this.breadcrumb.id_s = 0;
        this.breadcrumb.id_c = 0;
        this.breadcrumb.id_m = 0;
        if (sessionStorage != null) {
            this.session = sessionStorage.getItem("token");
            this.userName = _auth.getCurrentUserName();
            this._dataservice.getPoints(sessionStorage.getItem('currentUserId')).subscribe(function (data) {
                _this.points = data.point_balance;
                console.log(data.point_balance);
            });
        }
        else {
            this.session = null;
            this.userName = '';
        }
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (sessionStorage.getItem('currentUserId') != null) {
            // this.points = this._dataservice.getPoints();
            this._dataservice.getPoints(sessionStorage.getItem('currentUserId')).subscribe(function (data) {
                _this.points = data.point_balance;
                console.log(data.point_balance);
                if (new Date() > new Date(data.points_expire_on)) {
                    sessionStorage.setItem('isPackageExpired', 'true');
                    sessionStorage.setItem('packageName', 'paid');
                }
                else {
                    sessionStorage.setItem('isPackageExpired', 'false');
                    sessionStorage.setItem('packageName', 'paid');
                }
            });
        }
        else {
            sessionStorage.setItem('packageName', 'trail');
        }
    };
    AppComponent.prototype.ngOnDestroy = function () {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    };
    AppComponent.prototype.logout = function () {
        this._auth.logout();
        this.router.navigate(['home']);
        location.reload();
    };
    AppComponent.prototype.setSubject = function (name) {
        // this._dataservice.setSubject(id,name);
        console.log(name);
        console.log(this.breadcrumb);
        //this.breadcrumb = this._dataservice.getBreadCrumbs()
        console.log(this.breadcrumb);
    };
    ;
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["MediaMatcher"],
            _service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__["DataserviceService"],
            _service_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/esm5/radio.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_slider__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/slider */ "./node_modules/@angular/material/esm5/slider.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/esm5/sidenav.es5.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/button-toggle */ "./node_modules/@angular/material/esm5/button-toggle.es5.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/esm5/chips.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/esm5/progress-spinner.es5.js");
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/progress-bar */ "./node_modules/@angular/material/esm5/progress-bar.es5.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/esm5/snack-bar.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm5/sort.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/esm5/scrolling.es5.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _service_dataservice_service__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./service/dataservice.service */ "./src/app/service/dataservice.service.ts");
/* harmony import */ var _components_module_videos_module_videos_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./components/module-videos/module-videos.component */ "./src/app/components/module-videos/module-videos.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var angular_font_awesome__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! angular-font-awesome */ "./node_modules/angular-font-awesome/dist/angular-font-awesome.es5.js");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./guards/auth.guard */ "./src/app/guards/auth.guard.ts");
/* harmony import */ var _service_authentication_service__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./service/authentication.service */ "./src/app/service/authentication.service.ts");
/* harmony import */ var _service_userService__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./service/userService */ "./src/app/service/userService.ts");
/* harmony import */ var _helpers_jwt_interceptor__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./helpers/jwt.interceptor */ "./src/app/helpers/jwt.interceptor.ts");
/* harmony import */ var _components_user_handling_login_login_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./components/user-handling/login/login.component */ "./src/app/components/user-handling/login/login.component.ts");
/* harmony import */ var _components_user_handling_register_register_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./components/user-handling/register/register.component */ "./src/app/components/user-handling/register/register.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_alert_service__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./service/alert.service */ "./src/app/service/alert.service.ts");
/* harmony import */ var _components_alert_alert_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./components/alert/alert.component */ "./src/app/components/alert/alert.component.ts");
/* harmony import */ var _helpers_textpipe__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./helpers/textpipe */ "./src/app/helpers/textpipe.ts");
/* harmony import */ var _helpers_validationpipe__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./helpers/validationpipe */ "./src/app/helpers/validationpipe.ts");
/* harmony import */ var _components_expire_expire_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./components/expire/expire.component */ "./src/app/components/expire/expire.component.ts");
/* harmony import */ var _components_chapter_chapter_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./components/chapter/chapter.component */ "./src/app/components/chapter/chapter.component.ts");



//Angular Material Components





















































// import {StarRatingComponent} from "./extraComponents/star-rating/star-rating.component";
// import {StarRatingDisplayComponent} from "./extraComponents/star-rating/StarRatingDisplayComponent";
// import { DeviceDetectorModule } from 'ngx-device-detector';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_35__["AppComponent"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_34__["routingComponents"],
                //fakeBackendProvider,
                _components_module_videos_module_videos_component__WEBPACK_IMPORTED_MODULE_37__["VideoPlayback"],
                _components_alert_alert_component__WEBPACK_IMPORTED_MODULE_48__["AlertComponent"],
                _components_user_handling_login_login_component__WEBPACK_IMPORTED_MODULE_44__["LoginComponent"],
                _components_user_handling_register_register_component__WEBPACK_IMPORTED_MODULE_45__["RegisterComponent"],
                _helpers_textpipe__WEBPACK_IMPORTED_MODULE_49__["ReplaceLineBreaks"],
                _helpers_validationpipe__WEBPACK_IMPORTED_MODULE_50__["ValidationMsgPipe"],
                _components_expire_expire_component__WEBPACK_IMPORTED_MODULE_51__["ExpireComponent"],
                _components_chapter_chapter_component__WEBPACK_IMPORTED_MODULE_52__["PopUp"]
                // StarRatingComponent,
                // StarRatingDisplayComponent
                //ScrollDispatchModule,
                // McBreadcrumbsModule.forRoot()
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_34__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_46__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_38__["HttpClientModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
                _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_6__["MatAutocompleteModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_7__["MatDatepickerModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__["MatFormFieldModule"],
                _angular_material_radio__WEBPACK_IMPORTED_MODULE_9__["MatRadioModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_10__["MatSelectModule"],
                _angular_material_slider__WEBPACK_IMPORTED_MODULE_11__["MatSliderModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_12__["MatSlideToggleModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_13__["MatMenuModule"],
                _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_14__["MatSidenavModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_15__["MatToolbarModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_16__["MatListModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_17__["MatGridListModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_18__["MatCardModule"],
                _angular_material_stepper__WEBPACK_IMPORTED_MODULE_19__["MatStepperModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_20__["MatTabsModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
                _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_22__["MatButtonToggleModule"],
                _angular_material_chips__WEBPACK_IMPORTED_MODULE_23__["MatChipsModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_24__["MatIconModule"],
                _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_25__["MatProgressSpinnerModule"],
                _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_26__["MatProgressBarModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_27__["MatDialogModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_28__["MatTooltipModule"],
                _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_29__["MatSnackBarModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_30__["MatTableModule"],
                _angular_material_sort__WEBPACK_IMPORTED_MODULE_31__["MatSortModule"],
                _angular_material_paginator__WEBPACK_IMPORTED_MODULE_32__["MatPaginatorModule"],
                angular_font_awesome__WEBPACK_IMPORTED_MODULE_39__["AngularFontAwesomeModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_34__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_38__["HttpClientModule"],
                _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_33__["ScrollingModule"],
                // DeviceDetectorModule.forRoot(),
                _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_33__["ScrollDispatchModule"]
            ],
            entryComponents: [
                _components_module_videos_module_videos_component__WEBPACK_IMPORTED_MODULE_37__["VideoPlayback"],
                _components_chapter_chapter_component__WEBPACK_IMPORTED_MODULE_52__["PopUp"]
            ],
            providers: [
                _service_dataservice_service__WEBPACK_IMPORTED_MODULE_36__["DataserviceService"],
                _guards_auth_guard__WEBPACK_IMPORTED_MODULE_40__["AuthGuard"],
                _service_alert_service__WEBPACK_IMPORTED_MODULE_47__["AlertService"],
                _service_authentication_service__WEBPACK_IMPORTED_MODULE_41__["AuthenticationService"],
                _service_userService__WEBPACK_IMPORTED_MODULE_42__["UserService"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_38__["HTTP_INTERCEPTORS"],
                    useClass: _helpers_jwt_interceptor__WEBPACK_IMPORTED_MODULE_43__["JwtInterceptor"],
                    multi: true
                },
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_35__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/alert/alert.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/alert/alert.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"message\" [ngClass]=\"{ 'alert': message, 'alert-success': message.type === 'success', 'alert-danger': message.type === 'error' }\">{{message.text}}</div>"

/***/ }),

/***/ "./src/app/components/alert/alert.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/alert/alert.component.ts ***!
  \*****************************************************/
/*! exports provided: AlertComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertComponent", function() { return AlertComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_alert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/alert.service */ "./src/app/service/alert.service.ts");

/**
 * Created by hp on 1/1/2019.
 */


var AlertComponent = /** @class */ (function () {
    function AlertComponent(alertService) {
        this.alertService = alertService;
    }
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.alertService.getMessage().subscribe(function (message) { _this.message = message; });
    };
    AlertComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            // moduleId: module.id,
            selector: 'alert',
            template: __webpack_require__(/*! ./alert.component.html */ "./src/app/components/alert/alert.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_service_alert_service__WEBPACK_IMPORTED_MODULE_2__["AlertService"]])
    ], AlertComponent);
    return AlertComponent;
}());



/***/ }),

/***/ "./src/app/components/chapter/chapter.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/chapter/chapter.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<cdk-virtual-scroll-viewport  style=\"height: 86vh\"  itemSize=\"100\" >\r\n    <div class=\"container \" >\r\n    <ng-container  *cdkVirtualFor=\"let x of chapter\">\r\n        <div  class=\"container-chapter\"  >\r\n            <button mat-raised-button color=\"primary\" (click)=\"onSelect(x)\"  class=\"button-chapter overme\">\r\n                <i class=\"fa fa-address-book\" aria-hidden=\"true\"></i> <span style=\"padding-left:10px;\" >{{x.name}}</span>\r\n                <i class=\"fa fa-lock pull-right lock\" *ngIf=\"x.is_unlock == 0\"></i>\r\n            </button>\r\n        </div>\r\n     </ng-container>\r\n    </div>\r\n</cdk-virtual-scroll-viewport>\r\n\r\n<style>\r\n.container{\r\n    padding-top:10px\r\n}\r\n/*.mat-dialog-container{*/\r\n    /*background-color:#222 !important;*/\r\n/*}*/\r\n.container-chapter{\r\n  min-width: 18%;\r\n  margin: 1% auto;\r\n  display: block;\r\n}\r\n\r\nbutton:hover {\r\n  color: #ffffff;\r\n  text-decoration: none;\r\n}\r\n.lock{\r\n    background: transparent !important;\r\n    color: #2ad322 !important;\r\n    box-shadow: 0 0 !important;\r\n    font-size: 19px !important;\r\n    text-shadow: 0px 0px 14px #2ad322 !important;\r\n}\r\n\r\n.button-chapter{\r\n  border-radius: 16px;\r\n  padding:20px;\r\n  font-weight: 500;\r\n  width: 100%;\r\n  text-align: left;\r\n  left: 0;\r\n    font-size: 14px;\r\n}\r\n\r\ni.fa {\r\n    display: inline-block;\r\n    border-radius: 60px;\r\n    box-shadow: 0px 0px 2px #2ad322;\r\n    background-color: #2ad322;\r\n    padding: 0.5em 0.6em;\r\n    color: black;\r\n}\r\n.overme {\r\n\r\n    overflow:hidden;\r\n    white-space:nowrap;\r\n    text-overflow: ellipsis;\r\n}\r\n@media only screen and (min-width : 320px) and (max-width : 480px) {\r\n    .container{\r\n        padding-left: 3vw;\r\n        width: 95vw;\r\n    }\r\n    .overme {\r\n        width: 90vw;\r\n   }\r\n}\r\n@media only screen and (min-width : 480px) and (max-width : 595px) {\r\n    .container{\r\n        padding-left: 24vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 595px) and (max-width : 690px) {\r\n    .container{\r\n        padding-left: 24vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 690px) and (max-width : 800px) {\r\n    .container {\r\n        padding-left: 24vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 800px) and (max-width : 1024px) {\r\n}\r\n@media only screen and (min-width : 1024px) and (max-width : 1224px) {\r\n\r\n}\r\n</style>"

/***/ }),

/***/ "./src/app/components/chapter/chapter.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/chapter/chapter.component.ts ***!
  \*********************************************************/
/*! exports provided: ChapterComponent, PopUp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChapterComponent", function() { return ChapterComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopUp", function() { return PopUp; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/dataservice.service */ "./src/app/service/dataservice.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../app.component */ "./src/app/app.component.ts");






var ChapterComponent = /** @class */ (function () {
    // public points = 0;
    function ChapterComponent(route, dialog, router, _dataservice, app) {
        this.route = route;
        this.dialog = dialog;
        this.router = router;
        this._dataservice = _dataservice;
        this.app = app;
        this.chapter = [];
        this.userId = 0;
        this.app.userName = sessionStorage.getItem('currentUserName');
        this.app.session = sessionStorage.getItem('token');
        app.breadcrumb.id_c = 0;
        app.breadcrumb.id_m = 0;
        this._dataservice.getBreadCrumbs();
        if (sessionStorage.getItem('currentUserId')) {
            this.userId = parseInt(sessionStorage.getItem('currentUserId'));
            // this._dataservice.getPoints(sessionStorage.getItem('currentUserId')).subscribe(data => {
            //     this.points = data.point_balance;
            //     console.log(data.point_balance);
            // });
        }
    }
    ChapterComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(sessionStorage);
        var id = parseInt(this.route.snapshot.paramMap.get('id'));
        this._dataservice.getChapters(id, this.userId).subscribe(function (data) {
            _this.chapter = data;
        });
        console.log(id);
        console.log(this._dataservice.getBreadCrumbs());
        console.log(this._dataservice.getBreadCrumbs().id_s);
        if (this._dataservice.getBreadCrumbs().id_s != id) {
            this.router.navigate(['/home']);
        }
    };
    ChapterComponent.prototype.onSelect = function (sel) {
        console.log(sel);
        this._dataservice.setChapter(sel.id, sel.name);
        if (sessionStorage.getItem('currentUserId')) {
            // if(sessionStorage.getItem('packageName') == null || sessionStorage.getItem('isPackageExpired')=="true"){
            if (sel.is_unlock <= 0) {
                this.openDialog(sel);
            }
            else {
                this._dataservice.setChapter(sel.id, sel.name);
                this.router.navigate(['/mod', sel.id]);
            }
        }
        else {
            this.router.navigate(['/mod', sel.id]);
        }
        // this.router.navigate(['/mod', sel.id]);
    };
    ChapterComponent.prototype.setSubject = function () {
    };
    ChapterComponent.prototype.safeUrl = function (url) {
        return this._dataservice.getSafeUrl(url);
    };
    // =====================
    ChapterComponent.prototype.openDialog = function (sel) {
        var dialogRef = this.dialog.open(PopUp, {
            disableClose: true,
            minWidth: '60%',
            minHeight: '40%',
            maxWidth: '90%',
            maxHeight: '90%',
            data: sel
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed');
        });
    };
    ChapterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-chapter',
            template: __webpack_require__(/*! ./chapter.component.html */ "./src/app/components/chapter/chapter.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__["DataserviceService"], _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]])
    ], ChapterComponent);
    return ChapterComponent;
}());

var PopUp = /** @class */ (function () {
    function PopUp(dialogRef, data, _dataservice, router) {
        this.dialogRef = dialogRef;
        this.data = data;
        this._dataservice = _dataservice;
        this.router = router;
        this.noty = '';
        this.chapterData = data;
    }
    PopUp.prototype.ngOnInit = function () {
        var _this = this;
        this._dataservice.getPackages().subscribe(function (result) {
            _this.packages = result;
            console.log(result);
        });
        if (sessionStorage.getItem('currentUserId')) {
            this._dataservice.getPoints(sessionStorage.getItem('currentUserId')).subscribe(function (data) {
                // this.points = data.point_balance;
                console.log(data.point_balance);
            });
        }
    };
    PopUp.prototype.selectPackage = function (x) {
        var _this = this;
        this._dataservice.selectPckageSave(this.chapterData.id, x, sessionStorage.getItem('currentUserId')).subscribe(function (data) {
            console.log(data.userDetails.point_balance);
            // this.app.points =data.userDetails.point_balance;
            if (data.status == true) {
                _this.noty = '<div class="alert alert-success alert-dismissible">' +
                    data.message +
                    '</div>';
                setTimeout(function () {
                    // this.router.navigate(['/home']);
                    location.reload(true);
                }, 3000);
            }
            else {
                _this.noty = '<div class="alert alert-danger alert-dismissible">' +
                    data.message +
                    '</div>';
            }
        });
        console.log(x);
    };
    PopUp = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'pop-up',
            template: __webpack_require__(/*! ./pop-dialog.html */ "./src/app/components/chapter/pop-dialog.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], Object, src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__["DataserviceService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], PopUp);
    return PopUp;
}());



/***/ }),

/***/ "./src/app/components/chapter/pop-dialog.html":
/*!****************************************************!*\
  !*** ./src/app/components/chapter/pop-dialog.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"background:#fff;color:rgb(96, 252, 96);\">\r\n\r\n    <h3 mat-dialog-title style=\"text-align: center\" class=\"package-title\">Packages</h3>\r\n    <mat-dialog-content class=\"mat-dialog-content\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12\" id=\"noty-sec\" [innerHTML]=\"noty\">\r\n                </div>\r\n\r\n                <ng-container *ngFor=\"let x of packages\">\r\n                    <div class=\"col-lg col-md col-sm col-xs-12 package-item\">\r\n                        <h2>{{x.title}}</h2>\r\n                        <p>{{x.description}}</p>\r\n                        <p>Period : {{x.period}} ({{x.duration}} Days)</p>\r\n                        <p>{{x.price}} Points</p>\r\n                        <button (click)=\"selectPackage(x.id)\" class=\"btn btn-info btn-sm\">Select</button>\r\n                        <!--*ngIf=\"x.price <= points\"-->\r\n                    </div>\r\n                </ng-container>\r\n            </div>\r\n    </mat-dialog-content>\r\n\r\n</div>\r\n<style>\r\n    mat-dialog-container {\r\n        padding-left: 0px;\r\n        padding-right: 0px;\r\n        background-color: #222 !important;\r\n    }\r\n\r\n    .mat-dialog-container {\r\n        background-color: #222 !important;\r\n    }\r\n\r\n    .package-item {\r\n        border: 1px solid #000;\r\n        margin: 2px;\r\n        padding: 15px;\r\n        border-radius: 8px;\r\n        color: #000;\r\n        text-align: center;\r\n    }\r\n\r\n    .package-title {\r\n        text-align: center;\r\n        margin: 0 0 10px;\r\n        font-size: 22px;\r\n        color: #000;\r\n    }\r\n\r\n    .clearly-understood {\r\n\r\n        text-align: center;\r\n        padding: 8px;\r\n    }\r\n\r\n    .understood {\r\n\r\n        text-align: center;\r\n        padding: 8px;\r\n    }\r\n\r\n    .confused {\r\n\r\n        text-align: center;\r\n        padding: 8px;\r\n    }\r\n\r\n    .not-understood {\r\n        text-align: center;\r\n        padding: 8px;\r\n    }\r\n\r\n    .custom-row {\r\n        padding-top: 15px;\r\n    }\r\n\r\n    .mat-dialog-content {\r\n        overflow: hidden !important;\r\n    }\r\n\r\n    .cdk-overlay-pane {\r\n        min-width: 90%;\r\n    }\r\n\r\n    @media only screen and (min-width: 320px) and (max-width: 480px) {\r\n        .col-sm-12 {\r\n            padding-bottom: 10px;\r\n        }\r\n       .package-item h2{\r\n           font-size: 16px !important;\r\n           margin: 0px 0px 4px;\r\n           font-weight: bold;\r\n       }\r\n       .package-item p{\r\n           font-size: 12px;\r\n           margin: 0px;\r\n       }\r\n       .package-item{\r\n           padding: 10px;\r\n       }\r\n       .package-item button{\r\n           margin-top: 4px;\r\n       }\r\n    }\r\n\r\n    @media only screen and (min-width: 480px) and (max-width: 595px) {\r\n\r\n    }\r\n\r\n    @media only screen and (min-width: 595px) and (max-width: 690px) {\r\n\r\n    }\r\n\r\n    @media only screen and (min-width: 690px) and (max-width: 800px) {\r\n\r\n    }\r\n\r\n    @media only screen and (min-width: 800px) and (max-width: 1024px) {\r\n        .clearly-understood {\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n\r\n        .understood {\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n\r\n        .confused {\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n\r\n        .not-understood {\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n    }\r\n\r\n    @media only screen and (min-width: 1024px) and (max-width: 1224px) {\r\n\r\n        .clearly-understood {\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n\r\n        .understood {\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n\r\n        .confused {\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n\r\n        .not-understood {\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n    }\r\n</style>"

/***/ }),

/***/ "./src/app/components/expire/expire.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/expire/expire.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<cdk-virtual-scroll-viewport class=\"view-port\" itemSize=\"100\" style=\"height: 80vh\">\r\n    <div class=\"container\">\r\n        <div class=\"inner-div\">\r\n            <h4>Your trial period was ended on {{expire_on|replaceLineBreaks}}</h4>\r\n            <img class=\"img-unhappy\" src=\"data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Y2lyY2xlIHN0eWxlPSJmaWxsOiNGRkQ5M0I7IiBjeD0iMjU2IiBjeT0iMjU2IiByPSIyNTYiLz4KPGc+Cgk8cGF0aCBzdHlsZT0iZmlsbDojRjRDNTM0OyIgZD0iTTUxMiwyNTZjMCwxNDEuNDQtMTE0LjY0LDI1Ni0yNTYsMjU2Yy04MC40OCwwLTE1Mi4zMi0zNy4xMi0xOTkuMjgtOTUuMjggICBjNDMuOTIsMzUuNTIsOTkuODQsNTYuNzIsMTYwLjcyLDU2LjcyYzE0MS4zNiwwLDI1Ni0xMTQuNTYsMjU2LTI1NmMwLTYwLjg4LTIxLjItMTE2LjgtNTYuNzItMTYwLjcyICAgQzQ3NC44LDEwMy42OCw1MTIsMTc1LjUyLDUxMiwyNTZ6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojRjRDNTM0OyIgZD0iTTI3Mi40NDgsMjMyLjI3MmMtMTYuNTYsNDMuNzQ0LDEwLjcwNCw5NC41OTIsNjAuOTEyLDExMy42MTYgICBjNTAuMDgsMTguOTYsMTA0LjE5Mi0xLjA1NiwxMjAuNzUyLTQ0LjhjMTYuNTEyLTQzLjYzMi0xMC43MzYtOTQuNDgtNjAuODMyLTExMy40NEMzNDMuMDcyLDE2OC42MDgsMjg4Ljk2LDE4OC42NCwyNzIuNDQ4LDIzMi4yNzIgICB6Ii8+CjwvZz4KPGNpcmNsZSBzdHlsZT0iZmlsbDojRkZGRkZGOyIgY3g9IjM1MS43MjgiIGN5PSIyNjEuODg4IiByPSI2OC4yMjQiLz4KPHBhdGggc3R5bGU9ImZpbGw6IzNFNDM0NzsiIGQ9Ik0zNzYuNDY0LDI2Ni4xNzZjMC4xMjgsMjIuMjU2LTE3LjgwOCw0MC4xOTItNDAuMDY0LDQwLjA2NGMtMjIuMTI4LDAtNDAuMTkyLTE3LjgwOC00MC4xOTItNDAuMDY0ICBzMTguMDY0LTQwLjA2NCw0MC4xOTItNDAuMDY0QzM1OC42NTYsMjI1Ljk2OCwzNzYuNDY0LDI0NC4wMzIsMzc2LjQ2NCwyNjYuMTc2eiIvPgo8ZWxsaXBzZSB0cmFuc2Zvcm09Im1hdHJpeCgtMC43MDcxIC0wLjcwNzEgMC43MDcxIC0wLjcwNzEgNDE5Ljc5MjEgNjc4LjI2NTcpIiBzdHlsZT0iZmlsbDojRkZGRkZGOyIgY3g9IjM1MC4zNjkiIGN5PSIyNTIuMTkxIiByeD0iMTAuOTQ0IiByeT0iNy40NCIvPgo8cGF0aCBzdHlsZT0iZmlsbDojRjRDNTM0OyIgZD0iTTIzOS41NTIsMjMyLjI3MmMxNi41Niw0My43NDQtMTAuNzA0LDk0LjU5Mi02MC45MTIsMTEzLjYxNiAgYy01MC4wOCwxOC45Ni0xMDQuMTkyLTEuMDU2LTEyMC43NTItNDQuOGMtMTYuNTI4LTQzLjYzMiwxMC43MzYtOTQuNDgsNjAuODE2LTExMy40NCAgQzE2OC45MjgsMTY4LjYwOCwyMjMuMDQsMTg4LjY0LDIzOS41NTIsMjMyLjI3MnoiLz4KPGNpcmNsZSBzdHlsZT0iZmlsbDojRkZGRkZGOyIgY3g9IjE2MC4zMiIgY3k9IjI2MS44ODgiIHI9IjY4LjIyNCIvPgo8cGF0aCBzdHlsZT0iZmlsbDojM0U0MzQ3OyIgZD0iTTIxNS43OTIsMjY2LjE3NmMwLjEyOCwyMi4yNTYtMTcuODA4LDQwLjE5Mi00MC4wNjQsNDAuMDY0Yy0yMi4xNDQsMC00MC4xOTItMTcuODA4LTQwLjE5Mi00MC4wNjQgIHMxOC4wNjQtNDAuMDY0LDQwLjE5Mi00MC4wNjRDMTk3Ljk4NCwyMjUuOTY4LDIxNS43OTIsMjQ0LjAzMiwyMTUuNzkyLDI2Ni4xNzZ6Ii8+CjxlbGxpcHNlIHRyYW5zZm9ybT0ibWF0cml4KC0wLjcwNzEgLTAuNzA3MSAwLjcwNzEgLTAuNzA3MSAxNDUuNDgwOSA1NjQuNjY0NikiIHN0eWxlPSJmaWxsOiNGRkZGRkY7IiBjeD0iMTg5LjY4NiIgY3k9IjI1Mi4yMDIiIHJ4PSIxMC45NDQiIHJ5PSI3LjQ0Ii8+CjxnPgoJPHBhdGggc3R5bGU9ImZpbGw6IzNFNDM0NzsiIGQ9Ik00MjcuMjgsMTI3Ljg0Yy0zNi45NDQsMjEuMzc2LTExNC44OTYsNTUuOTUyLTE0NS40NzItMTAuMzg0Yy0xLjQ3Mi0zLjIsMi4yMjQtNi4yODgsNS4xMi00LjM1MiAgIGMxNy4yMTYsMTEuMTg0LDU4LjI4OCwyNy43MTIsMTM3LjY0OCw4LjI1NkM0MjguNTc2LDEyMC4zODQsNDMwLjgxNiwxMjUuNzEyLDQyNy4yOCwxMjcuODR6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojM0U0MzQ3OyIgZD0iTTgyLjYyNCwxNDkuMDg4YzQyLjM4NCw1LjY0OCwxMjcuNzQ0LDcuODU2LDEzMC40MTYtNjUuMTg0YzAuMTI4LTMuNTItNC40OC00Ljk2LTYuNDE2LTIuMDY0ICAgYy0xMS41ODQsMTYuOTEyLTQzLjE4NCw0Ny45MDQtMTI0LjAxNiw2MC4yMjRDNzguNTQ0LDE0Mi42ODgsNzguNTI4LDE0OC40OCw4Mi42MjQsMTQ5LjA4OHoiLz4KCTxwYXRoIHN0eWxlPSJmaWxsOiMzRTQzNDc7IiBkPSJNMzYzLjI2NCw0NDQuNDE2Yy00LjY1Ni0xNC44NDgtMjEuNDg4LTM1LjEwNC02OC45MTItNTAuOTEyICAgYy0yNC4zMDQtOS4zMTItNzQuNjU2LTE4LjcwNC04OC42ODgtMjIuMjRjMjAuNTYtNS4xMiw2MS41ODQtMTguODgsOTcuOTItNi40NDhDMzM2LjkyOCwzNzcuNzkyLDM2My4yNjQsNDA3LjA4OCwzNjMuMjY0LDQ0NC40MTZ6ICAgIi8+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==\" />\r\n            <h5>Please contact us to activate your account</h5>\r\n            <h6>Call Us</h6>\r\n            <h6><b>077 977 1993</b></h6>\r\n            <h6><b>076 658 9424</b></h6>\r\n            <h6><b>075 799 8745</b></h6>\r\n            <h6><b>075 240 4332</b></h6>\r\n            <br>\r\n            <h6>SchoolX WhatsApp <b> 0757998745</b></h6>\r\n            <p>\r\n                <a [routerLink]=\"['/home']\" class=\"btn btn-register\">\r\n                    <button class=\"btn\">Back to Home</button>\r\n                </a>\r\n\r\n            </p>\r\n        </div>\r\n    </div>\r\n</cdk-virtual-scroll-viewport>\r\n<style>\r\n    .inner-div{\r\n        width: 100%;\r\n        background: #fff;\r\n        padding: 20px 0px;\r\n    }\r\n    .img-unhappy{\r\n        width: 200px;\r\n        margin: 0px auto;\r\n        display: block;\r\n    }\r\n    h4{\r\n        text-align: center;\r\n        margin-top: 10px;\r\n        padding-top: 25px;\r\n        padding-bottom: 30px;\r\n        font-size: 30px;\r\n        color: #c80606;\r\n    }\r\n    h5{\r\n        text-align: center;\r\n        padding-top: 20px;\r\n        padding-bottom: 20px;\r\n        color: #535050;\r\n    }\r\n    h6{\r\n        text-align: center;\r\n        color: #535050;\r\n    }\r\n    .example-container{\r\n        background: #fff !important;\r\n    }\r\n    .ng-star-inserted{\r\n        display: none !important;\r\n    }\r\n    p{\r\n        text-align: center;\r\n        padding-top: 40px;\r\n        padding-bottom: 40px;\r\n    }\r\n    .btn{\r\n        background: #2ad322;\r\n        color: #fff;\r\n        outline: none;\r\n    }\r\n</style>"

/***/ }),

/***/ "./src/app/components/expire/expire.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/expire/expire.component.ts ***!
  \*******************************************************/
/*! exports provided: ExpireComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpireComponent", function() { return ExpireComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/app.component */ "./src/app/app.component.ts");




var ExpireComponent = /** @class */ (function () {
    function ExpireComponent(router, route, app) {
        this.router = router;
        this.route = route;
        this.app = app;
        this.expire = 'false';
        this.expire_on = '';
        this.app.session = sessionStorage.getItem('token');
        if (sessionStorage.getItem('isExpire')) {
            this.expire = sessionStorage.getItem('isExpire');
        }
        if (sessionStorage.getItem('expire_on')) {
            this.expire_on = sessionStorage.getItem('expire_on');
        }
    }
    ExpireComponent.prototype.ngOnInit = function () {
        if (!this.expire === true) {
            this.router.navigate(['/login']);
            location.reload();
        }
    };
    ExpireComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            // selector: 'expire',
            template: __webpack_require__(/*! ./expire.component.html */ "./src/app/components/expire/expire.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]])
    ], ExpireComponent);
    return ExpireComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<cdk-virtual-scroll-viewport class=\"view-port\"   itemSize=\"100\" >\r\n<img src=\"assets/img/banner_image2.jpg\" alt=\"\" style=\"width:100%;max-height:50vh;\">\r\n<div class=\"container\">\r\n    <div class=\"grade\">\r\n    <button mat-raised-button class=\"button-grade\" color=\"primary\" (click)=\"onSelect(1)\">GRADE <span class=\"grade-num\">8</span></button>\r\n    <button mat-raised-button class=\"button-grade\" color=\"primary\" (click)=\"onSelect(2)\">GRADE <span class=\"grade-num\">9</span></button>\r\n    <button mat-raised-button class=\"button-grade\" color=\"primary\" (click)=\"onSelect(3)\">GRADE <span class=\"grade-num\">10</span></button>\r\n    <button mat-raised-button class=\"button-grade\" color=\"primary\" (click)=\"onSelect(4)\">GRADE <span class=\"grade-num\">11</span></button>\r\n    </div>\r\n  </div>\r\n\r\n</cdk-virtual-scroll-viewport>\r\n<style>\r\n    .view-port{\r\n        height:80vh;\r\n    }\r\n.container{\r\n    padding-top:10px;\r\n    height: 70vh;\r\n    max-width: 1440px;\r\n}\r\n.button-grade{\r\n  border-radius: 16px;\r\n  padding:20px;\r\n  min-width: 18%;\r\n  font-weight: 500;\r\n  margin: 1%;\r\n  min-width: 180px;\r\n}\r\n\r\nbutton:hover {\r\n  color: #ffffff;\r\n  text-decoration: none;\r\n} \r\n\r\n.grade-num{\r\n  font-size:300%; \r\n  font-weight:800;\r\n  padding:5px;\r\n}\r\n@media only screen and (min-width : 320px) and (max-width : 480px) {\r\n    .container{\r\n        padding-left: 24vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 480px) and (max-width : 595px) {\r\n    .container{\r\n        padding-left: 24vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 595px) and (max-width : 690px) {\r\n    .container{\r\n        padding-left: 24vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 690px) and (max-width : 800px) {\r\n    .container {\r\n        padding-left: 24vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 800px) and (max-width : 1024px) {\r\n    .container {\r\n        padding-left: 5vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 1024px) and (max-width : 1224px) {\r\n    .container {\r\n        padding-left: 10vw;\r\n    }\r\n}@media only screen and (min-width : 1225px) {\r\n    .container {\r\n        padding-left: 20vw;\r\n    }\r\n}\r\n</style>"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/dataservice.service */ "./src/app/service/dataservice.service.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../app.component */ "./src/app/app.component.ts");





var HomeComponent = /** @class */ (function () {
    function HomeComponent(route, router, _dataservice, app) {
        this.route = route;
        this.router = router;
        this._dataservice = _dataservice;
        this.app = app;
        this.breadcrumb = {};
        this.grade = "";
        this.app.userName = sessionStorage.getItem('currentUserName');
        this.app.session = sessionStorage.getItem('token');
        app.breadcrumb.id_g = 0;
        app.breadcrumb.id_s = 0;
        app.breadcrumb.id_c = 0;
        app.breadcrumb.id_m = 0;
    }
    HomeComponent.prototype.ngOnInit = function () {
        this._dataservice.getBreadCrumbs();
    };
    HomeComponent.prototype.onSelect = function (id) {
        this.router.navigate(['/sub', id]);
        if (id == 1) {
            this.grade = "8";
        }
        else if (id == 2) {
            this.grade = "9";
        }
        else if (id == 3) {
            this.grade = "10";
        }
        else if (id == 4) {
            this.grade = "11";
        }
        this._dataservice.setGrade(id, "Grade " + this.grade);
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__["DataserviceService"], _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/module-videos/module-videos.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/module-videos/module-videos.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<cdk-virtual-scroll-viewport  style=\"height: 80vh\"  itemSize=\"100\">\r\n<div class=\"row\" style=\"width:95%; margin:auto;padding:0px\" >\r\n  <div class=\"col-lg-2 col-md-6 col-sm-12\">\r\n    <button  class=\" bnt-primary btn-xs btn-stat clearly-understood\" (click)=\"onRatingModuleChanged(1)\">\r\n        <i class=\"material-icons\">sentiment_very_satisfied</i> Clearly Understood\r\n    </button>\r\n  </div>\r\n\r\n  <div class=\"col-lg-2 col-md-6 col-sm-12\">\r\n    <button class=\"btn-stat understood bnt-primary btn-xs \" (click)=\"onRatingModuleChanged(2)\">\r\n        <i class=\"material-icons\" style=\"padding-left: 25px\">sentiment_satisfied</i>Understood\r\n    </button>\r\n  </div>\r\n\r\n  <div class=\"col-lg-2 col-md-6 col-sm-12\">\r\n    <button class=\"btn-stat confused bnt-primary btn-xs\" (click)=\"onRatingModuleChanged(3)\">\r\n        <i class=\"material-icons\" style=\"padding-left: 33px\">sentiment_dissatisfied</i> Confused\r\n    </button>\r\n  </div>\r\n\r\n  <div class=\"col-lg-2 col-md-6 col-sm-12\">\r\n    <button class=\"btn-stat not-understood bnt-primary btn-xs\" (click)=\"onRatingModuleChanged(4)\" >\r\n        <i class=\"material-icons\" style=\"padding-left: 5px\">sentiment_very_dissatisfied </i> Didn't Understand\r\n    </button>\r\n  </div>\r\n\r\n  <div class=\"col-lg-4 col-md-12 col-sm-12\">\r\n    <a  class=\"btn-stat bnt-primary btn-xs\" style=\"text-align: left;padding: 8px; color: #2ad322;\r\nbackground-color: black;\" [matMenuTriggerFor]=\"tutor\" >\r\n      <mat-icon>arrow_drop_down</mat-icon> Select Tutor </a>      \r\n  </div>\r\n</div>\r\n\r\n<mat-menu #tutor=\"matMenu\">\r\n  <a mat-menu-item>Tutor 01</a>\r\n  <a mat-menu-item>Tutor 02</a>\r\n  <a mat-menu-item>Tutor 03</a>\r\n  <a mat-menu-item>Tutor 04</a>\r\n</mat-menu>\r\n\r\n\r\n    <div class=\"container \" >\r\n    <div class=\"row\" >\r\n    <ng-container *cdkVirtualFor=\"let x of modulevideo\">\r\n      <div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\">\r\n        <div class=\"card-module-video\">\r\n          <mat-card color=\"primary\">\r\n            <mat-card-header>\r\n              <mat-card-title>{{x.name}}</mat-card-title>\r\n              <mat-card-subtitle>by {{x.tutor}}</mat-card-subtitle>\r\n            </mat-card-header>\r\n              <!--<img mat-card-image (click)=\"openDialog(x)\" src=\"https://www.myaone.my/images/no-image-lesson.jpg\" alt=\"Video not available\">-->\r\n              <img mat-card-image (click)=\"openDialog(x)\" src=\"{{(x.image)? x.image : 'http://sx.adrotec.com/oie_twjHAbUBQYYr.jpg'}}\" alt=\"\">\r\n              <!--<img mat-card-image (click)=\"openDialog(x)\" src='https://img.youtube.com/vi/{{x.video.split(\"https://www.youtube.com/watch?v=\").pop()}}/default.jpg' alt=\"Video not available\">-->\r\n            <mat-card-content>\r\n              <p>Clearly Understood <span class=\"pull-right\">\r\n                <div class=\"circle\" style=\"background-color: green;\">\r\n                  <span class=\"width_holder\">{{x.cun ? x.cun : 0}}</span>\r\n                  <span class=\"num\">{{x.cun ? x.cun : 0}}</span>\r\n                </div>\r\n                </span>\r\n              </p>\r\n              <div style=\"height: 4px;\"></div>\r\n              <p>Understood <span class=\"pull-right\">\r\n               <div class=\"circle\" style=\"background-color: #e6e618;\" >\r\n                  <span class=\"width_holder\">{{x.und ? x.und : 0}}</span>\r\n                  <span class=\"num\">{{x.und ? x.und : 0}}</span>\r\n                </div>\r\n              </span>\r\n              </p>\r\n              <div style=\"height: 4px;\"></div>\r\n              <p>Confused <span class=\"pull-right\">\r\n               <div class=\"circle\" style=\"background-color: orange;\">\r\n                  <span class=\"width_holder\">{{x.con ? x.con : 0}}</span>\r\n                  <span class=\"num\">{{x.con ? x.con : 0}}</span>\r\n                </div>\r\n              </span>\r\n              </p>\r\n              <div style=\"height: 4px;\"></div>\r\n              <p>Didn't Understand <span class=\"pull-right\">\r\n                <div class=\"circle\" style=\"background-color: red;\">\r\n                  <span class=\"width_holder\">{{x.dun ? x.dun : 0}}</span>\r\n                  <span class=\"num\">{{x.dun ? x.dun : 0}}</span>\r\n                </div>\r\n              </span>\r\n              </p>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </div> \r\n      </div>\r\n    </ng-container>\r\n    </div>\r\n\r\n</div>\r\n\r\n</cdk-virtual-scroll-viewport>\r\n\r\n<style>\r\n.card-module-video{\r\n  width: 100%;\r\n  margin: 4% auto;\r\n  display: inline-block;\r\n}\r\n\r\n.btn-stat{\r\n  border-radius: 16px;\r\n  min-width: 98%;\r\n  margin: 1% auto;\r\n    display: inline-flex;\r\n    align-items: center;\r\n    height: 35px;\r\n}\r\n\r\nbutton:hover {\r\n  color: #ffffff;\r\n  text-decoration: none;\r\n}\r\n\r\n\r\n.circle {\r\n  display: inline-block;\r\n  border-radius: 50%;\r\n  position: relative;\r\n  padding: 10px;\r\n  margin-bottom: 0px;\r\n  float: right;\r\n    color: white;\r\n}\r\n.circle::after {\r\n  content: '';\r\n  display: block;\r\n  padding-bottom: 100%;\r\n  height: 0;\r\n  opacity: 0;\r\n}\r\n.num {\r\n  position: absolute;\r\n  top: 50%;\r\n  transform: translateY(-50%);\r\n}\r\n.width_holder {\r\n  display: block;\r\n  height: 0;\r\n  overflow: hidden;\r\n}\r\n.clearly-understood{\r\n  color: green;\r\n    text-align: center;\r\n    padding: 8px;\r\n    background-color: grey;\r\n}\r\n  .understood{\r\n    color: yellow;\r\n      text-align: center;\r\n      padding: 8px;\r\n      background-color: grey;\r\n  }\r\n  .confused {\r\n    color: orange;\r\n      text-align: center;\r\n      padding: 8px;\r\n      background-color: grey;\r\n  }\r\n  .not-understood{\r\n      color: red;\r\n      text-align: center;\r\n      padding: 8px;\r\n      background-color: grey;\r\n  }\r\n\r\n\r\n@media only screen and (min-width : 320px) and (max-width : 480px) {\r\n    .clearly-understood{\r\n        padding-left: 17vw;\r\n    }\r\n    .understood{\r\n        padding-left: 14vw;\r\n    }\r\n    .confused {\r\n        padding-left: 15vw;\r\n    }\r\n    .not-understood{\r\n        padding-left: 16vw;;\r\n    }\r\n}\r\n@media only screen and (min-width : 480px) and (max-width : 595px) {\r\n    .clearly-understood{\r\n        padding-left: 22vw;\r\n    }\r\n    .understood{\r\n        padding-left: 22vw;\r\n    }\r\n    .confused {\r\n        padding-left: 20vw;\r\n    }\r\n    .not-understood{\r\n        padding-left: 20vw;;\r\n    }\r\n}\r\n@media only screen and (min-width : 595px) and (max-width : 690px) {\r\n    .clearly-understood{\r\n        padding-left: 22vw;\r\n    }\r\n    .understood{\r\n        padding-left: 22vw;\r\n    }\r\n    .confused {\r\n        padding-left: 20vw;\r\n    }\r\n    .not-understood{\r\n        padding-left: 20vw;;\r\n    }\r\n}\r\n@media only screen and (min-width : 690px) and (max-width : 800px) {\r\n.row{\r\n    padding-top: 2vh !important;\r\n}\r\n}\r\n@media only screen and (min-width : 800px) and (max-width : 1024px) {\r\n\r\n}\r\n@media only screen and (min-width : 1024px) and (max-width : 1224px) {\r\n    .btn-stat{\r\n        font-size: 10px !important;\r\n    }\r\n}\r\n@media only screen and (min-width : 1225px) {\r\n    .btn-stat{\r\n        font-size: 10px !important;\r\n    }\r\n}\r\n</style>"

/***/ }),

/***/ "./src/app/components/module-videos/module-videos.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/module-videos/module-videos.component.ts ***!
  \*********************************************************************/
/*! exports provided: ModuleVideosComponent, VideoPlayback */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModuleVideosComponent", function() { return ModuleVideosComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoPlayback", function() { return VideoPlayback; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/dataservice.service */ "./src/app/service/dataservice.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/app.component */ "./src/app/app.component.ts");






var ModuleVideosComponent = /** @class */ (function () {
    function ModuleVideosComponent(dialog, router, route, _dataservice, app) {
        this.dialog = dialog;
        this.router = router;
        this.route = route;
        this._dataservice = _dataservice;
        this.app = app;
        this.modulevideo = [];
        this.module = 1;
        this.userId = 0;
        this.expire = 'false';
        this.app.userName = sessionStorage.getItem('currentUserName').replace(/['"]+/g, '');
        this.app.session = sessionStorage.getItem('token');
        if (sessionStorage.getItem('currentUserId')) {
            this.userId = parseInt(sessionStorage.getItem('currentUserId'));
        }
        // if (sessionStorage.getItem('isExpire')) {
        //     this.expire = sessionStorage.getItem('isExpire');
        // }
    }
    ModuleVideosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isRated = false;
        console.log(sessionStorage);
        // if (this.expire === 'true') {
        //     this.router.navigate(['/expire']);
        //     // location.reload();
        // }
        var id = parseInt(this.route.snapshot.paramMap.get('id'));
        this._dataservice.getModuleVideos(id, this.userId).subscribe(function (data) {
            _this.modulevideo = data;
            console.log(data);
        });
        this.module = id;
        if (this._dataservice.getBreadCrumbs().id_m !== id) {
            this.router.navigate(['/home']);
        }
    };
    ModuleVideosComponent.prototype.openDialog = function (sel) {
        var _this = this;
        var dialogRef = this.dialog.open(VideoPlayback, {
            disableClose: true,
            minWidth: '60%',
            minHeight: '40%',
            maxWidth: '90%',
            maxHeight: '90%',
            data: sel
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed');
            var id = parseInt(_this.route.snapshot.paramMap.get('id'));
            _this._dataservice.getModuleVideos(id, _this.userId).subscribe(function (data) {
                _this.modulevideo = data;
            });
        });
    };
    ModuleVideosComponent.prototype.onRatingModuleChanged = function (rating) {
        this.isRated = true;
        this._dataservice.setModuleRating(this.module, rating).subscribe(function (result) {
            console.log(result);
        });
    };
    ModuleVideosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-module-videos',
            template: __webpack_require__(/*! ./module-videos.component.html */ "./src/app/components/module-videos/module-videos.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__["DataserviceService"],
            src_app_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]])
    ], ModuleVideosComponent);
    return ModuleVideosComponent;
}());

var VideoPlayback = /** @class */ (function () {
    function VideoPlayback(dialogRef, data, _dataservice, router) {
        this.dialogRef = dialogRef;
        this.data = data;
        this._dataservice = _dataservice;
        this.router = router;
    }
    VideoPlayback.prototype.ngOnInit = function () {
        this.isRated = false;
        this.modulevideo = this.data;
        this.videoSrc = this._dataservice.getSafeUrl('https://www.youtube.com/embed/' + this.modulevideo.video.split("https://www.youtube.com/watch?v=").pop());
    };
    VideoPlayback.prototype.onRatingVideoChanged = function (rating) {
        this.isRated = true;
        this._dataservice.setVideosRating(this.modulevideo.id, rating).subscribe(function (result) {
            console.log(result);
        });
        //  this.router.navigate([['/mod',this.modulevideo.id]]);
    };
    VideoPlayback = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'video-playback',
            template: __webpack_require__(/*! ./video-dialog.html */ "./src/app/components/module-videos/video-dialog.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], Object, src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__["DataserviceService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], VideoPlayback);
    return VideoPlayback;
}());



/***/ }),

/***/ "./src/app/components/module-videos/video-dialog.html":
/*!************************************************************!*\
  !*** ./src/app/components/module-videos/video-dialog.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div style=\"background:white;color:rgb(96, 252, 96);\">\r\n    <h3 mat-dialog-title>{{modulevideo.name}}</h3>\r\n    <mat-dialog-content class=\"mat-dialog-content\">\r\n        <div class=\"embed-responsive embed-responsive-16by9\" style=\"width:100%;margin:auto 0;\">\r\n            <iframe class=\"embed-responsive-item\" [src]=\"videoSrc\" frameborder=\"0\" allowfullscreen scrolling=\"no\"></iframe>\r\n        </div>\r\n    </mat-dialog-content>\r\n    <mat-dialog-actions align=\"center\" class=\"row custom-row\">\r\n        <span *ngIf=\"!isRated\" class=\"col-md-3 col-sm-12\"><button style=\"width:100%; background-color: green;color: white\"  mat-flat-button (click)=\"onRatingVideoChanged(4)\" class=\"btn-stat clearly-understood\"><mat-icon>sentiment_very_satisfied</mat-icon>  Clearly Understood</button></span>\r\n        <span *ngIf=\"!isRated\" class=\"col-md-3 col-sm-12\"><button style=\"width:100%; background-color: #e6e618;color: white\" mat-flat-button (click)=\"onRatingVideoChanged(3)\" class=\"btn-stat understood\"><mat-icon>sentiment_satisfied</mat-icon>  Understood</button></span>\r\n        <span *ngIf=\"!isRated\" class=\"col-md-3 col-sm-12\"><button style=\"width:100%; background-color: orange;color: white\" mat-flat-button (click)=\"onRatingVideoChanged(2)\" class=\"btn-stat confused\"><mat-icon>sentiment_dissatisfied</mat-icon> Confused</button></span>\r\n        <span *ngIf=\"!isRated\" class=\"col-md-3 col-sm-12\"><button style=\"width:100%; background-color: red;color: white\"  mat-flat-button (click)=\"onRatingVideoChanged(1)\" class=\"btn-stat not-understood\"><mat-icon>sentiment_very_dissatisfied</mat-icon>  Didn't Understand</button></span>\r\n        <span *ngIf=\"isRated\" style=\"width:100%;margin: auto;\">\r\n            <button mat-raised-button color=\"warn\" mat-dialog-close style=\"width:100%;\" >Close</button>\r\n        </span>\r\n    </mat-dialog-actions>\r\n</div>\r\n\r\n<style>\r\n    mat-dialog-container{\r\n        padding-left: 0px;\r\n        padding-right: 0px;\r\n    }\r\n    .clearly-understood{\r\n\r\n        text-align: center;\r\n        padding: 8px;\r\n    }\r\n    .understood{\r\n\r\n        text-align: center;\r\n        padding: 8px;\r\n    }\r\n    .confused {\r\n\r\n        text-align: center;\r\n        padding: 8px;\r\n    }\r\n    .not-understood{\r\n         text-align: center;\r\n        padding: 8px;\r\n    }\r\n    .custom-row{\r\n        padding-top: 15px;\r\n    }\r\n    .mat-dialog-content{\r\n        overflow: hidden !important;\r\n    }\r\n.cdk-overlay-pane{\r\n    min-width:90%;\r\n}\r\n    @media only screen and (min-width : 320px) and (max-width : 480px) {\r\n        .col-sm-12{padding-bottom: 10px;}\r\n    }\r\n    @media only screen and (min-width : 480px) and (max-width : 595px) {\r\n\r\n    }\r\n    @media only screen and (min-width : 595px) and (max-width : 690px) {\r\n\r\n    }\r\n    @media only screen and (min-width : 690px) and (max-width : 800px) {\r\n\r\n    }\r\n    @media only screen and (min-width : 800px) and (max-width : 1024px) {\r\n        .clearly-understood{\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n        .understood{\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n        .confused {\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n        .not-understood{\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n    }\r\n    @media only screen and (min-width : 1024px) and (max-width : 1224px) {\r\n\r\n        .clearly-understood{\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n        .understood{\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n        .confused {\r\n\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n        .not-understood{\r\n            text-align: center;\r\n            padding: 8px;\r\n            width: 14vw;\r\n            font-size: 10px;\r\n        }\r\n    }\r\n</style>"

/***/ }),

/***/ "./src/app/components/module/module.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/module/module.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<cdk-virtual-scroll-viewport  style=\"height: 530px\"  itemSize=\"10\" >\r\n<div class=\"container\" style=\"padding-top:10px;\">\r\n    <ng-container *cdkVirtualFor=\"let x of module\">\r\n        <div class=\"container-module\">\r\n        <button mat-raised-button color=\"primary\" (click)=\"onSelect(x)\"  class=\"button-module overme \">\r\n            <i class=\"fa fa-paperclip\" aria-hidden=\"true\"></i> <span style=\"padding-left:10px;\">{{x.name}}</span>\r\n        </button>\r\n    </div>\r\n        </ng-container>\r\n</div>\r\n</cdk-virtual-scroll-viewport>\r\n<style>\r\n\r\n.container-module{\r\n  min-width: 18%;\r\n  margin: 1% auto;\r\n  display: block;\r\n}\r\n\r\n.button-module{\r\n  border-radius: 16px;\r\n  padding:20px;\r\n  /* background:green; */\r\n  /* color:white; */\r\n  font-weight: 500;\r\n  width: 100%;\r\n  text-align: left;\r\n  left: 0;\r\n    font-size: 14px;\r\n}\r\n\r\nbutton:hover {\r\n  color: #ffffff;\r\n  text-decoration: none;\r\n}\r\ni.fa {\r\n    display: inline-block;\r\n    border-radius: 60px;\r\n    box-shadow: 0px 0px 2px #2ad322;\r\n    background-color: #2ad322;\r\n    padding: 0.5em 0.6em;\r\n    color: black;\r\n}\r\n\r\n.overme {\r\n\r\n    overflow:hidden;\r\n    white-space:nowrap;\r\n    text-overflow: ellipsis;\r\n}\r\n@media only screen and (min-width : 320px) and (max-width : 480px) {\r\n    .container{\r\n        padding-left: 3vw;\r\n        width: 95vw;\r\n    }\r\n    .overme {\r\n        width: 90vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 480px) and (max-width : 595px) {\r\n    .container{\r\n        padding-left: 24vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 595px) and (max-width : 690px) {\r\n    .container{\r\n        padding-left: 24vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 690px) and (max-width : 800px) {\r\n    .container {\r\n        padding-left: 24vw;\r\n    }\r\n}\r\n@media only screen and (min-width : 800px) and (max-width : 1024px) {\r\n}\r\n@media only screen and (min-width : 1024px) and (max-width : 1224px) {\r\n\r\n}\r\n</style>"

/***/ }),

/***/ "./src/app/components/module/module.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/module/module.component.ts ***!
  \*******************************************************/
/*! exports provided: ModuleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModuleComponent", function() { return ModuleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/dataservice.service */ "./src/app/service/dataservice.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../app.component */ "./src/app/app.component.ts");





var ModuleComponent = /** @class */ (function () {
    function ModuleComponent(route, router, _dataservice, app) {
        this.route = route;
        this.router = router;
        this._dataservice = _dataservice;
        this.app = app;
        this.module = [];
        this.userId = 0;
        app.breadcrumb.id_m = 0;
        this.app.userName = sessionStorage.getItem('currentUserName');
        this.app.session = sessionStorage.getItem('token');
        if (sessionStorage.getItem('currentUserId')) {
            this.userId = parseInt(sessionStorage.getItem('currentUserId'));
        }
    }
    ModuleComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = parseInt(this.route.snapshot.paramMap.get('id'));
        this._dataservice.getModules(id, this.userId).subscribe(function (data) {
            console.log(data);
            _this.module = data;
        });
        console.log(this._dataservice.getBreadCrumbs());
        if (this._dataservice.getBreadCrumbs().id_c !== id) {
            this.router.navigate(['/home']);
        }
    };
    ModuleComponent.prototype.onSelect = function (sel) {
        this.router.navigate(['/modv', sel.id]);
        this._dataservice.setModule(sel.id, sel.name);
    };
    ModuleComponent.prototype.safeUrl = function (url) {
        return this._dataservice.getSafeUrl(url);
    };
    ModuleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-module',
            template: __webpack_require__(/*! ./module.component.html */ "./src/app/components/module/module.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_2__["DataserviceService"], _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]])
    ], ModuleComponent);
    return ModuleComponent;
}());



/***/ }),

/***/ "./src/app/components/subject/subject.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/subject/subject.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<cdk-virtual-scroll-viewport  style=\"height: 530px\"  itemSize=\"10\" >\r\n<div class=\"container\" style=\"padding-top:10px;\">\r\n    <ng-container *cdkVirtualFor=\"let x of subjects\">\r\n    <div class=\"container-subject\">\r\n        <button mat-raised-button color=\"primary\" (click)=\"onSelect(x)\" class=\"button-subject\">\r\n\r\n               <i class=\"fa fa-briefcase\" aria-hidden=\"true\"></i> <span style=\"padding-left:10px;\">{{x.name}}</span>\r\n        </button>\r\n        <br>\r\n    </div>\r\n        </ng-container>\r\n</div>\r\n</cdk-virtual-scroll-viewport>\r\n<style>\r\n\r\n.container-subject{\r\n  min-width: 100%;\r\n  margin: 1%;\r\n  display: inline-block;\r\n}\r\n\r\n.button-subject{\r\n  border-radius: 16px;\r\n  padding:20px;\r\n  font-weight: 500;\r\n  min-width: 100%;\r\n  text-align: left;\r\n  left: 0;\r\n    font-size: 14px;\r\n}\r\nbutton:hover {\r\n    color: #ffffff;\r\n    text-decoration: none;\r\n}\r\n\r\ni.fa {\r\n    display: inline-block;\r\n    border-radius: 60px;\r\n    box-shadow: 0px 0px 2px #2ad322;\r\n    background-color: #2ad322;\r\n    padding: 0.5em 0.6em;\r\n    color: black;\r\n}\r\n@media only screen and (max-width: 400px) {\r\n    body {\r\n        //background-color: lightblue;\r\n    }\r\n}\r\n\r\n</style>"

/***/ }),

/***/ "./src/app/components/subject/subject.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/subject/subject.component.ts ***!
  \*********************************************************/
/*! exports provided: SubjectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubjectComponent", function() { return SubjectComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/dataservice.service */ "./src/app/service/dataservice.service.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../app.component */ "./src/app/app.component.ts");





var SubjectComponent = /** @class */ (function () {
    function SubjectComponent(route, router, _dataservice, app) {
        this.route = route;
        this.router = router;
        this._dataservice = _dataservice;
        this.app = app;
        this.subjects = [];
        this.userId = 0;
        this.app.userName = sessionStorage.getItem('currentUserName');
        this.app.session = sessionStorage.getItem('token');
        app.breadcrumb.id_s = 0;
        app.breadcrumb.id_c = 0;
        app.breadcrumb.id_m = 0;
        this._dataservice.getBreadCrumbs();
        if (sessionStorage.getItem('currentUserId')) {
            this.userId = parseInt(sessionStorage.getItem('currentUserId'));
        }
    }
    SubjectComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = parseInt(this.route.snapshot.paramMap.get('id'));
        this._dataservice.getSubjects(id, this.userId).subscribe(function (data) {
            _this.subjects = data;
        });
        if (this._dataservice.getBreadCrumbs().id_g !== id) {
            this.router.navigate(['/home']);
        }
    };
    SubjectComponent.prototype.onSelect = function (sel) {
        this.router.navigate(['/chap', sel.is]);
        this._dataservice.setSubject(sel.is, sel.name);
    };
    SubjectComponent.prototype.safeUrl = function (url) {
        return this._dataservice.getSafeUrl(url);
    };
    SubjectComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-subject',
            template: __webpack_require__(/*! ./subject.component.html */ "./src/app/components/subject/subject.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_service_dataservice_service__WEBPACK_IMPORTED_MODULE_3__["DataserviceService"], _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]])
    ], SubjectComponent);
    return SubjectComponent;
}());



/***/ }),

/***/ "./src/app/components/user-handling/login/login.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/user-handling/login/login.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<cdk-virtual-scroll-viewport class=\"view-port\"   itemSize=\"100\" >\r\n<div class=\"container\">\r\n  <mat-card color=\"primary\" class=\"custom-form\">\r\n    <mat-card-header>\r\n      <mat-card-title class=\"custom-title\"><h2>Login</h2></mat-card-title>\r\n      </mat-card-header>\r\n    <mat-card-content>\r\n      <form name=\"form\" (ngSubmit)=\"f.form.valid && login()\" #f=\"ngForm\" novalidate>\r\n        <!--<div *ngIf=\"error.error\" style=\"color: red\" >UnAuthorised Access</div>-->\r\n        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !email.valid }\">\r\n          <label for=\"username\">Email</label>\r\n          <input type=\"text\" class=\"form-control\" name=\"email\" [(ngModel)]=\"model.email\" #email=\"ngModel\" required email/>\r\n          <!--<div *ngIf=\"f.submitted && !email.valid\" class=\"help-block\">email is required</div>-->\r\n          <div *ngIf=\"email.errors?.required && email.touched\" style = \"color: red;\" class=\"help-block\">The Email is required</div>\r\n          <div *ngIf=\"email.errors?.email && email.touched\" style = \"color: red;\">The email must be a valid email address</div>\r\n\r\n        </div>\r\n        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !password.valid }\">\r\n          <label for=\"password\">Password</label>\r\n          <input type=\"password\" class=\"form-control\" name=\"password\" [(ngModel)]=\"model.password\" #password=\"ngModel\" required minlength=\"6\"/>\r\n          <!--<div *ngIf=\"f.submitted && !password.valid\" class=\"help-block\">Password is required</div>-->\r\n          <div *ngIf=\"password.errors?.required && password.touched\" class=\"help-block\" style = \"color: red;\">The Password is required</div>\r\n          <div *ngIf=\"password.errors?.minlength && password.touched\" class=\"help-block\" style = \"color: red;\">The password must be at least 6 characters</div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n          <div *ngIf=\"!f.form.valid\" style=\"color: #fff421 !important; font-weight: 300;\">Fill the form to continue!</div>\r\n          <p style = \"color: red;\" >{{error_msg}}</p>\r\n          <button [disabled]=\"loading\" class=\"btn btn-login btn-sm\">Login</button>\r\n          <img *ngIf=\"loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\" />\r\n          <a [routerLink]=\"['/register']\" class=\"btn btn-register\"><u>Register</u></a>\r\n        </div>\r\n      </form>\r\n      </mat-card-content>\r\n   </mat-card>\r\n\r\n\r\n</div>\r\n</cdk-virtual-scroll-viewport>\r\n<style>\r\n  .view-port{\r\n    height:86vh;\r\n  }\r\n  .custom-form{\r\n    width: 100%;\r\n    background-color: Black;\r\n    color: #2ad322;\r\n  }\r\n\r\n  .btn-login{\r\n    background-color: #2ad322;\r\n    border-color: #2ad322;\r\n    float: right;\r\n    color: black;\r\n  }\r\n  .btn-login:hover{\r\n    color: white;\r\n  }\r\n  .btn-register{\r\n    color: #2ad322;\r\n  }\r\n  .btn-register:hover{\r\n    color: white;\r\n  }\r\n  .custom-title{\r\n    margin-left: 5vw\r\n  }\r\n  @media only screen and (max-width : 319px){\r\n    .container{\r\n      padding-right: 2vw;\r\n      padding-left: 2vw;\r\n      padding-top: 2vh;\r\n      padding-bottom: 12vh;\r\n    }\r\n    .custom-title{\r\n      margin-left: 5vw\r\n    }\r\n\r\n  }\r\n  @media only screen and (min-width : 320px) and (max-width : 480px) {\r\n    .container{\r\n      padding-right: 5vw;\r\n      padding-left: 5vw;\r\n      padding-top: 80px;\r\n      padding-bottom: 80vh;\r\n\r\n    }\r\n  }\r\n  @media only screen and (min-width : 480px) and (max-width : 595px) {\r\n    .container{\r\n      padding-right: 5vw;\r\n      padding-left: 5vw;\r\n      padding-top: 80px;\r\n      padding-bottom: 80px;\r\n      padding-bottom: 80vh;\r\n    }\r\n  }\r\n  @media only screen and (min-width : 595px) and (max-width : 690px) {\r\n    .container{\r\n      padding-right: 5vw;\r\n      padding-left: 5vw;\r\n      padding-top: 80px;\r\n      padding-bottom: 80vh;\r\n    }\r\n  }\r\n  @media only screen and (min-width : 690px) and (max-width : 800px) {\r\n    .container {\r\n      padding-right: 10vw;\r\n      padding-left: 10vw;\r\n      padding-top: 25vh;\r\n      padding-bottom: 35vh;\r\n    }\r\n  }\r\n  @media only screen and (min-width : 800px) and (max-width : 1024px) {\r\n    .container {\r\n      padding-right: 10vw;\r\n      padding-left: 10vw;\r\n      padding-top: 25vh;\r\n      padding-bottom: 35vh;\r\n    }\r\n  }\r\n  @media only screen and (min-width : 1024px) and (max-width : 1224px) {\r\n    .container {\r\n      padding-right: 15vw;\r\n      padding-left: 15vw;\r\n      padding-top: 25vh;\r\n      padding-bottom: 45vh;\r\n    }\r\n  }\r\n  @media only screen and (min-width : 1225px) {\r\n    .container {\r\n      padding-right: 15vw;\r\n      padding-left: 15vw;\r\n      padding-top: 25vh;\r\n      padding-bottom: 45vh;\r\n    }\r\n    .custom-title{\r\n      margin-left: 20vw\r\n    }\r\n  }\r\n\r\n</style>"

/***/ }),

/***/ "./src/app/components/user-handling/login/login.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/user-handling/login/login.component.ts ***!
  \*******************************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_service_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/authentication.service */ "./src/app/service/authentication.service.ts");
/* harmony import */ var src_app_service_alert_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/alert.service */ "./src/app/service/alert.service.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../app.component */ "./src/app/app.component.ts");






var LoginComponent = /** @class */ (function () {
    function LoginComponent(route, router, app, authenticationService, alertService) {
        this.route = route;
        this.router = router;
        this.app = app;
        this.authenticationService = authenticationService;
        this.alertService = alertService;
        this.model = {};
        this.loading = false;
        this.error_msg = '';
    }
    LoginComponent.prototype.ngOnInit = function () {
        // reset login status
        // this.authenticationService.logout();
        this.returnUrl = '/home';
        if (sessionStorage.getItem('currentUserId')) {
            // get return url from route parameters or default to '/'
            this.router.navigate([this.returnUrl]);
        }
        // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.loading = true;
        this.authenticationService.login(this.model.email, this.model.password)
            .subscribe(function (data) {
            location.reload(true);
            // window.location='http://schoolx.vasityprojects.com/home';
            // this.router.navigate([this.returnUrl]);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
            _this.error_msg = 'Invalid credentials. Please try again.';
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            //moduleId: module.id,
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/user-handling/login/login.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            src_app_service_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"],
            src_app_service_alert_service__WEBPACK_IMPORTED_MODULE_4__["AlertService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/user-handling/register/register.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/user-handling/register/register.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<cdk-virtual-scroll-viewport  class=\"view-port\"  itemSize=\"100\" >\r\n<div class=\"container\">\r\n  <mat-card color=\"primary\" class=\"custom-form\">\r\n    <mat-card-header>\r\n      <mat-card-title class=\"custom-title\"><h2>Register</h2></mat-card-title>\r\n    </mat-card-header>\r\n    <mat-card-content>\r\n\r\n      <form name=\"form\" (ngSubmit)=\"f.form.valid && register()\" #f=\"ngForm\" novalidate>\r\n        <div class=\"form-group\" [class.has-error]=\"email.invalid && email.touched \">\r\n          <label for=\"email\">Email</label>\r\n          <input type=\"email\" class=\"form-control\" name=\"email\" [(ngModel)]=\"model.email\" #email=\"ngModel\" required  email />\r\n          <div *ngIf=\"email.errors?.required && email.touched\" style = \"color: red;\" class=\"help-block\">The Email is required</div>\r\n          <div *ngIf=\"email.errors?.email && email.touched\" style = \"color: red;\">The email must be a valid email address</div>\r\n\r\n        </div>\r\n        <div class=\"form-group\" [class.has-error]=\"mobile.invalid && mobile.touched \">\r\n          <label for=\"mobile\">Mobile</label>\r\n          <input type=\"text\" class=\"form-control\" name=\"mobile\" [(ngModel)]=\"model.mobile\" #mobile=\"ngModel\" required minlength=\"10\"/>\r\n          <div *ngIf=\"mobile.errors?.required && mobile.touched\" class=\"help-block\" style = \"color: red;\">The mobile is required</div>\r\n          <div *ngIf=\"mobile.errors?.minlength && mobile.touched\" class=\"help-block\" style = \"color: red;\">The mobile must be at least 10 characters</div>\r\n        </div>\r\n        <div class=\"form-group\" [class.has-error]=\"username.invalid && username.touched \" [class.has-sucess]=\"username.invalid\">\r\n          <label for=\"name\" class=\"control-lable\">Name</label>\r\n          <input type=\"text\" class=\"form-control\" name=\"name\" [(ngModel)]=\"model.name\" #username=\"ngModel\" required minlength=\"3\" />\r\n          <span *ngIf=\"username.errors?.required && username.touched \" class=\"help-block\" style = \"color: red;\">The name is required</span>\r\n          <span *ngIf=\"username.errors?.minlength  && username.touched \" class=\"help-block\" style = \"color: red;\">The name must be at least 3 characters</span>\r\n        </div>\r\n        <div class=\"form-group\" [class.has-error]=\"password.invalid && password.touched \" >\r\n          <label for=\"password\">Password</label>\r\n          <input type=\"password\" class=\"form-control\" name=\"password\" [(ngModel)]=\"model.password\" #password=\"ngModel\" required minlength=\"6\"/>\r\n          <div *ngIf=\"password.errors?.required && password.touched\" class=\"help-block\" style = \"color: red;\">The Password is required</div>\r\n          <div *ngIf=\"password.errors?.minlength && password.touched\" class=\"help-block\" style = \"color: red;\">The password must be at least 6 characters</div>\r\n        </div>\r\n<!--        <div class=\"form-group\" [class.has-error]=\"confirmPassword.invalid && confirmPassword.touched \" >\r\n          <label for=\"confirmPassword\">Confirm Password</label>\r\n          <input type=\"password\" class=\"form-control\" name=\"confirmPassword\"  #confirmPassword required validateEqual=”password”/>\r\n          <div *ngIf=\"confirmPassword.errors?.required && confirmPassword.touched\" class=\"help-block\">The Password is required</div>\r\n          <div *ngIf=\"confirmPassword.errors?.mustMatch && confirmPassword.touched\" class=\"help-block\">The password must match</div>\r\n        </div-->\r\n<!--        <span *ngIf=\"errorSet\">\r\n          <p>{{errorSet?.errors|json|ValidationMsgPipe}}</p>\r\n        </span>-->\r\n        <p style = \"color: red;\" >{{error_msg}}</p>\r\n        <p style = \"color: green;\">{{success_msg}}</p>\r\n        <div *ngIf=\"!f.form.valid\" style=\"color: #fff421 !important; font-weight: 300;\">Fill the form to continue!</div>\r\n        <div *ngIf=\"f.form.valid\" style=\"color: #fff421 !important; font-weight: 300;\">Press register button to finish!</div>\r\n        <div class=\"form-group\">\r\n          <button [disabled]=\"loading\" class=\"btn btn-register btn-sm\">Register</button>\r\n          <img *ngIf=\"loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\" />\r\n          <a [routerLink]=\"['/login']\" class=\"btn btn-cancel\">Cancel</a>\r\n        </div>\r\n      </form>\r\n      </mat-card-content>\r\n    </mat-card>\r\n\r\n\r\n</div>\r\n  </cdk-virtual-scroll-viewport>\r\n<style>\r\n  .custom-form{\r\n    width: 100%;\r\n    background-color: Black;\r\n    color: #2ad322;\r\n  }\r\n  .view-port{\r\n    height:86vh;\r\n  }\r\n  .btn-register{\r\n    background-color: #2ad322;\r\n    border-color: #2ad322;\r\n    float: right;\r\n    color: black;\r\n  }\r\n  .btn-register:hover{\r\n    color: white;\r\n  }\r\n  .btn-cancel{\r\n    color: #2ad322;\r\n  }\r\n  .btn-cancel:hover{\r\n    color: white;\r\n  }\r\n  @media only screen and (max-width : 319px){\r\n    .container{\r\n      padding-right: 2vw;\r\n      padding-left: 2vw;\r\n      padding-top: 2vh;\r\n      padding-bottom: 12vh;\r\n    }\r\n    .custom-title{\r\n      margin-left: 5vw\r\n    }\r\n    .form-control{height: 5vh;}\r\n\r\n  }\r\n    @media only screen and (min-width : 320px) and (max-width : 480px) {\r\n    .container{\r\n      padding-right: 5vw;\r\n      padding-left: 5vw;\r\n      padding-top: 2vh;\r\n      padding-bottom: 10vh;\r\n    }\r\n    .custom-title{\r\n      margin-left: 20vw\r\n    }\r\n    .form-control{height: 5vh;}\r\n  }\r\n  @media only screen and (min-width : 480px) and (max-width : 595px) {\r\n    .container{\r\n      padding-right: 5vw;\r\n      padding-left: 5vw;\r\n      padding-top: 80px;\r\n      padding-bottom: 80px;\r\n      padding-bottom: 80vh;\r\n    }\r\n    .custom-title{\r\n      margin-left: 15vw;\r\n    }\r\n    .view-port{\r\n      height:86vh;\r\n    }\r\n  }\r\n  @media only screen and (min-width : 595px) and (max-width : 690px) {\r\n    .container{\r\n      padding-right: 5vw;\r\n      padding-left: 5vw;\r\n      padding-top: 80px;\r\n      padding-bottom: 80vh;\r\n    }\r\n    .custom-title{\r\n      margin-left: 15vw;\r\n    }\r\n  }\r\n  @media only screen and (min-width : 690px) and (max-width : 800px) {\r\n    .container {\r\n      padding-right: 10vw;\r\n      padding-left: 10vw;\r\n      padding-top: 25vh;\r\n      padding-bottom: 35vh;\r\n    }\r\n    .custom-title{\r\n      margin-left: 22vw;\r\n    }\r\n  }\r\n  @media only screen and (min-width : 800px) and (max-width : 1024px) {\r\n    .container {\r\n      padding-right: 10vw;\r\n      padding-left: 10vw;\r\n      padding-top: 25vh;\r\n      padding-bottom: 35vh;\r\n    }\r\n    .custom-title{\r\n      margin-left: 15vw;\r\n    }\r\n  }\r\n  @media only screen and (min-width : 1024px) and (max-width : 1224px) {\r\n    .container {\r\n      padding-right: 15vw;\r\n      padding-left: 15vw;\r\n      padding-top: 15vh;\r\n      padding-bottom: 45vh;\r\n    }\r\n    .custom-title{\r\n      margin-left: 15vw;\r\n    }\r\n  }\r\n\r\n  @media only screen and (min-width : 1225px) {\r\n    .container {\r\n      padding-right: 15vw;\r\n      padding-left: 15vw;\r\n      padding-top: 10vh;\r\n      padding-bottom: 45vh;\r\n    }\r\n    .custom-title{\r\n      margin-left: 20vw;\r\n    }\r\n  }\r\n</style>"

/***/ }),

/***/ "./src/app/components/user-handling/register/register.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/user-handling/register/register.component.ts ***!
  \*************************************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_service_alert_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/alert.service */ "./src/app/service/alert.service.ts");
/* harmony import */ var _service_userService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../service/userService */ "./src/app/service/userService.ts");





var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(router, userService, alertService) {
        this.router = router;
        this.userService = userService;
        this.alertService = alertService;
        this.error_msg = '';
        this.success_msg = '';
        this.model = {};
        this.loading = false;
        this.errorSet = { errors: {} };
        this.emailError = "";
    }
    RegisterComponent.prototype.register = function () {
        var _this = this;
        this.loading = true;
        this.userService.create(this.model)
            .subscribe(function (data) {
            console.log(data);
            _this.errorSet = data;
            ///this.emailError =this.errorSet.errors.email[0];
            if (_this.errorSet == 'true') {
                _this.success_msg = "Registration successful. Please Login to system.";
                _this.error_msg = "";
                setTimeout(function () {
                    _this.router.navigate(['/login']);
                }, 4000);
            }
            else {
                _this.success_msg = "";
                // console.log("lllll");
                // this.alertService.error('Email already exist...', true);
                _this.error_msg = 'Email already exist...';
            }
            _this.loading = false;
            // sessionStorage.setItem('token',data.token)
            // set success message and pass true paramater to persist the message after redirecting to the login page
            // this.alertService.success('Registration successful', true);
            // this.router.navigate(['/login']);
        }, function (error) {
            _this.alertService.error(error);
            _this.loading = false;
        });
    };
    RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            // moduleId: module.id,
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/components/user-handling/register/register.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _service_userService__WEBPACK_IMPORTED_MODULE_4__["UserService"],
            src_app_service_alert_service__WEBPACK_IMPORTED_MODULE_3__["AlertService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/guards/auth.guard.ts":
/*!**************************************!*\
  !*** ./src/app/guards/auth.guard.ts ***!
  \**************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (sessionStorage.getItem('token')) {
            // logged in so return true
            //window.location.reload();
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/helpers/jwt.interceptor.ts":
/*!********************************************!*\
  !*** ./src/app/helpers/jwt.interceptor.ts ***!
  \********************************************/
/*! exports provided: JwtInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return JwtInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var JwtInterceptor = /** @class */ (function () {
    function JwtInterceptor() {
    }
    JwtInterceptor.prototype.intercept = function (request, next) {
        // add authorization header with jwt token if available
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + currentUser.token
                }
            });
        }
        return next.handle(request);
    };
    JwtInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], JwtInterceptor);
    return JwtInterceptor;
}());



/***/ }),

/***/ "./src/app/helpers/textpipe.ts":
/*!*************************************!*\
  !*** ./src/app/helpers/textpipe.ts ***!
  \*************************************/
/*! exports provided: ReplaceLineBreaks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReplaceLineBreaks", function() { return ReplaceLineBreaks; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

/**
 * Created by hp on 1/5/2019.
 */

var ReplaceLineBreaks = /** @class */ (function () {
    function ReplaceLineBreaks() {
    }
    ReplaceLineBreaks.prototype.transform = function (value) {
        return value.replace(/"/g, '');
    };
    ReplaceLineBreaks = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({ name: 'replaceLineBreaks' })
    ], ReplaceLineBreaks);
    return ReplaceLineBreaks;
}());



/***/ }),

/***/ "./src/app/helpers/validationpipe.ts":
/*!*******************************************!*\
  !*** ./src/app/helpers/validationpipe.ts ***!
  \*******************************************/
/*! exports provided: ValidationMsgPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidationMsgPipe", function() { return ValidationMsgPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

/**
 * Created by hp on 1/6/2019.
 */

var ValidationMsgPipe = /** @class */ (function () {
    function ValidationMsgPipe() {
    }
    ValidationMsgPipe.prototype.transform = function (value) {
        //console.log(value)
        return "";
    };
    ValidationMsgPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({ name: 'ValidationMsgPipe' })
    ], ValidationMsgPipe);
    return ValidationMsgPipe;
}());



/***/ }),

/***/ "./src/app/service/alert.service.ts":
/*!******************************************!*\
  !*** ./src/app/service/alert.service.ts ***!
  \******************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");




var AlertService = /** @class */ (function () {
    function AlertService(router) {
        var _this = this;
        this.router = router;
        this.subject = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.keepAfterNavigationChange = false;
        // clear alert message on route change
        router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationStart"]) {
                if (_this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    _this.keepAfterNavigationChange = false;
                }
                else {
                    // clear alert
                    _this.subject.next();
                }
            }
        });
    }
    AlertService.prototype.success = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
    };
    AlertService.prototype.error = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message });
    };
    AlertService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    AlertService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AlertService);
    return AlertService;
}()); /**
 * Created by hp on 1/1/2019.
 */



/***/ }),

/***/ "./src/app/service/authentication.service.ts":
/*!***************************************************!*\
  !*** ./src/app/service/authentication.service.ts ***!
  \***************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");




var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http) {
        this.http = http;
        this.host = "http://vasityprojects.com/schoolx_api/api/";
    }
    // private host : string = "http://localhost:8000/api/";
    // private host : string = "http://sxapi.adrotec.com/api/";
    AuthenticationService.prototype.login = function (email, password) {
        return this.http.post(this.host + 'login', { email: email, password: password })
            .map(function (user) {
            // login successful if there's a jwt token in the response
            if (user && user.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                sessionStorage.setItem('token', JSON.stringify(user.token));
                sessionStorage.setItem('currentUserName', JSON.stringify(user.user["name"]));
                sessionStorage.setItem('currentUserId', JSON.stringify(user.user["id"]));
                sessionStorage.setItem('expire_on', JSON.stringify(user.user["expire_on"]));
                var today = new Date();
                // let user_expire_date = new Date();
                // if(JSON.stringify(user.user["expire_on"]) != ''){
                // }
                // let formatted_date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                if (JSON.stringify(user.user["expire_on"]) === "null") {
                    sessionStorage.setItem('isExpire', 'false');
                }
                else {
                    var user_expire_date = new Date(JSON.stringify(user.user["expire_on"]));
                    if (new Date() > user_expire_date) {
                        sessionStorage.setItem('isExpire', 'true');
                    }
                    else {
                        sessionStorage.setItem('isExpire', 'false');
                    }
                }
                console.log(sessionStorage);
            }
            return user;
        });
    };
    AuthenticationService.prototype.getCurrentUserId = function () {
        if (sessionStorage.getItem('currentUserId')) {
            return JSON.parse(localStorage.getItem('currentUserId'));
        }
        else
            return null;
    };
    AuthenticationService.prototype.getCurrentUserName = function () {
        if (sessionStorage.getItem('currentUserName')) {
            return sessionStorage.getItem('currentUserName');
        }
        else
            return "";
    };
    AuthenticationService.prototype.logout = function () {
        // remove user from local storage to log user out
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('currentUserName');
        sessionStorage.removeItem('currentUserId');
    };
    AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/service/dataservice.service.ts":
/*!************************************************!*\
  !*** ./src/app/service/dataservice.service.ts ***!
  \************************************************/
/*! exports provided: DataserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataserviceService", function() { return DataserviceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");





var DataserviceService = /** @class */ (function () {
    function DataserviceService(http, sanitizer) {
        this.http = http;
        this.sanitizer = sanitizer;
        this.userId = 0;
        //private host : string = "http://api.schoolxlk.tk/api/";
        this.env = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"];
        this.host = this.env.apiUrl;
    }
    DataserviceService.prototype.getSafeUrl = function (url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    };
    DataserviceService.prototype.getBreadCrumbs = function () {
        return breadcrumb;
    };
    DataserviceService.prototype.setGrade = function (id, name) {
        breadcrumb.id_g = id;
        breadcrumb.name_g = name;
        this.setSubject(0, "-");
        this.setChapter(0, "-");
        this.setModule(0, "-");
    };
    DataserviceService.prototype.setSubject = function (id, name) {
        breadcrumb.id_s = id;
        breadcrumb.name_s = name;
        this.setChapter(0, "-");
        this.setModule(0, "-");
    };
    DataserviceService.prototype.setChapter = function (id, name) {
        breadcrumb.id_c = id;
        breadcrumb.name_c = name;
        this.setModule(0, "-");
    };
    DataserviceService.prototype.setModule = function (id, name) {
        breadcrumb.id_m = id;
        breadcrumb.name_m = name;
    };
    DataserviceService.prototype.setModuleRating = function (id, rating) {
        return this.http.get(this.host + "modulerate/" + id + "/" + sessionStorage.getItem('currentUserId') + "/" + rating);
    };
    DataserviceService.prototype.setVideosRating = function (id, rating) {
        return this.http.get(this.host + "videorate/" + id + "/" + sessionStorage.getItem('currentUserId') + "/" + rating);
    };
    DataserviceService.prototype.getGrades = function () {
        return this.http.get(this.host + "grades/all");
    };
    DataserviceService.prototype.getSubjects = function (id, userId) {
        return this.http.get(this.host + "subjects/get/grade/" + id + "/" + userId);
    };
    DataserviceService.prototype.getChapters = function (id, userId) {
        return this.http.get(this.host + "chapters/get/subject/" + id + "/" + userId);
    };
    DataserviceService.prototype.getModules = function (id, userId) {
        //json:JSON=JSON.parse([{id:7,video:"https://www.youtube.com/watch?v=FPhetlu3f2g",name:"Another Video",cun:31,und:30,con:21,dun:19}])
        //return this.json;
        return this.http.get(this.host + "modules/get/chapter/" + id + "/" + userId);
    };
    DataserviceService.prototype.getModuleVideos = function (id, userId) {
        return this.http.get(this.host + "materials/get/module/" + id + "/" + userId);
    };
    DataserviceService.prototype.getPoints = function (userId) {
        return this.http.get(this.host + "points/get/" + userId);
    };
    DataserviceService.prototype.getPackages = function () {
        return this.http.get(this.host + "packages/get/all");
    };
    DataserviceService.prototype.selectPckageSave = function (chapter_id, package_id, userId) {
        // console.log(chapter_id);
        return this.http.get(this.host + "package/select/" + chapter_id + "/" + package_id + "/" + userId);
    };
    DataserviceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])
    ], DataserviceService);
    return DataserviceService;
}());

var breadcrumb = {
    "id_g": 0,
    "id_s": 0,
    "id_c": 0,
    "id_m": 0,
    "name_g": "-",
    "name_s": "-",
    "name_c": "-",
    "name_m": "-"
};
;


/***/ }),

/***/ "./src/app/service/userService.ts":
/*!****************************************!*\
  !*** ./src/app/service/userService.ts ***!
  \****************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.env = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"];
        this.host = this.env.apiUrl;
    }
    UserService.prototype.getAll = function () {
        return this.http.get('/api/users');
    };
    UserService.prototype.getById = function (id) {
        return this.http.get('/api/users/' + id);
    };
    UserService.prototype.create = function (user) {
        return this.http.post(this.host + 'register1', user);
    };
    UserService.prototype.update = function (user) {
        return this.http.put('/api/users/' + user.id, user);
    };
    UserService.prototype.delete = function (id) {
        return this.http.delete('/api/users/' + id);
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    // apiUrl:"http://localhost:8000/api/"
    apiUrl: "http://vasityprojects.com/schoolx_api/api/"
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! F:\web development\wfx\schoolxlk\front\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map